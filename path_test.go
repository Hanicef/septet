
package septet

import (
	"testing"
)

func checkAbsolute(t *testing.T, path, expect string) {
	path = client.Absolute(path)
	if path != expect {
		t.Errorf("Absolute failed! Expected %s but got %s.", expect, path)
	}
}

func TestAbsolute(t *testing.T) {
	client.current = &client.root
	checkAbsolute(t, "foo", "/foo")
	checkAbsolute(t, "bar/", "/bar")
	checkAbsolute(t, "/loren", "/loren")
	checkAbsolute(t, "loren/ipsum", "/loren/ipsum")
	client.current = client.root.Get("bar").(*Directory)
	checkAbsolute(t, "baz", "/bar/baz")
	checkAbsolute(t, "foo/", "/bar/foo")
	checkAbsolute(t, "/ipsum", "/ipsum")
	checkAbsolute(t, "/", "/")
	checkAbsolute(t, "foo/baz", "/bar/foo/baz")
}

func TestPathBody(t *testing.T) {
	if PathBody("/foo/bar") != "/foo" {
		t.Error("PathBody('/foo/bar') failed!")
	}
	if PathBody("/foo/bar/") != "/foo" {
		t.Error("PathBody('/foo/bar/') failed!")
	}
	if PathBody("foo/bar") != "foo" {
		t.Error("PathBody('foo/bar') failed!")
	}
	if PathBody("foo") != "foo" {
		t.Error("PathBody('foo') failed!")
	}
	if PathBody("/foo") != "/" {
		t.Error("PathBody('/foo') failed!")
	}
	if PathBody("/") != "/" {
		t.Error("PathBody('/') failed!")
	}
}

func checkSplitPath(t *testing.T, split string, expectPath string, expect string) {
	dir, name := client.splitPath(split)
	if dir.Path() != expectPath {
		t.Errorf("splitPath failed! Expected path %s but got %s.", expectPath, dir.Path())
	} else if name != expect {
		t.Errorf("splitPath failed! Expected %s but got %s.", expect, name)
	}
}

func TestSplitPath(t *testing.T) {
	checkSplitPath(t, "/foo", "/", "foo")
	checkSplitPath(t, "/bar/baz", "/bar", "baz")
	checkSplitPath(t, "/", "/", "/")
	client.current = client.root.Get("bar").(*Directory)
	checkSplitPath(t, "baz", "/bar", "baz")
	checkSplitPath(t, "baz/file", "/bar/baz", "file")
	checkSplitPath(t, "file", "/bar", "file")
}
