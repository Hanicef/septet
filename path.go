
package septet

import (
	"fmt"
	"strings"
)

// Absolute returns the absolute path of path within the client tree.
func (c *Client) Absolute(path string) string {
	if len(path) == 0 {
		return ""
	}

	path = strings.TrimRight(path, "/")
	if len(path) == 0 {
		return "/"
	}

	if path[0] == '/' {
		return path
	} else {
		if c.current.ID() == 0 {
			return fmt.Sprint("/", path)
		} else {
			return fmt.Sprint(c.current.Path(), "/", path)
		}
	}
}

func (c *Client) splitPath(path string) (Entry, string) {
	path = strings.TrimRight(path, "/")
	if len(path) == 0 {
		return &c.root, "/"
	}

	par := strings.LastIndexByte(path, '/')
	if par == -1 {
		return c.current, path
	} else if par == 0 {
		return &c.root, path[1:]
	} else {
		return c.Locate(path[:par]), path[par+1:]
	}
}

// PathHead returns a sliced string of the uppermost name of path.
func PathHead(path string) string {
	par := strings.LastIndexByte(strings.TrimRight(path, "/"), '/')
	if par == -1 {
		return path
	} else {
		return path[par+1:]
	}
}

// PathBody returns a sliced string of the path up to the uppermost name of
// path; that is, it removes the rightmost name of the path. If the path is
// the root path or is relative with no directories, path itself is retured.
func PathBody(path string) string {
	par := strings.LastIndexByte(strings.TrimRight(path, "/"), '/')
	if par == 0 {
		return "/"
	} else if par == -1 {
		return path
	}

	return path[:par]
}

// Relocate moves the client to a specific location. This is useful if a client
// should end up in a different directory when navigating.
func (c *Client) Relocate(path string) error {
	dir := c.Locate(path)
	if dir == nil {
		return ErrNotExist
	} else if !dir.Directory() {
		return ErrNotDirectory
	}
	c.current = dir.(*Directory)
	return nil
}
