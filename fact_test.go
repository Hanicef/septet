
package septet

import (
	"testing"
	"bytes"
)

func TestFormatFacts(t *testing.T) {
	buf := []byte{}
	for i := 0; i < 1<<uint(len(factList)); i++ {
		buf = buf[:0]
		for j := 0; j < len(factList); j++ {
			buf = append(buf, factList[j]...)
			if 1<<uint(j) & i != 0 {
				buf = append(buf, '*')
			}
			buf = append(buf, ';')
		}

		got := formatFacts(i)
		if bytes.Compare(buf, got) != 0 {
			t.Errorf("formatFacts failed! Expected %s but got %s.", buf, got)
		}
	}
}

func TestParseFacts(t *testing.T) {
	buf := []byte{}
	for i := 0; i < 1<<uint(len(factList)); i++ {
		buf = buf[:0]
		for j := 0; j < len(factList); j++ {
			if 1<<uint(j) & i != 0 {
				buf = append(buf, factList[j]...)
				buf = append(buf, ';')
			}
		}

		mask, got := parseFacts(string(buf))
		if bytes.Compare(got, buf) != 0 {
			t.Errorf("parseFacts failed! Expected %s but got %s.", buf, got)
		} else if mask != i {
			t.Errorf("parseFacts failed! Expected %d but got %d.", i, mask)
		}
	}
}
