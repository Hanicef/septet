
package main

import (
	"gitlab.com/Hanicef/septet"
	"time"
	"strconv"
	"net"
	"io"
	"fmt"
)

const ReadCount = 64

type User struct {
	Password string
	RootEntries []septet.Entry
}

var Users = make(map[string]*User)

func (u *User) AddEntry(entry septet.Entry) {
	u.RootEntries = append(u.RootEntries, entry)
}

func (u *User) RemoveEntry(entry septet.Entry) {
	for i := range u.RootEntries {
		if u.RootEntries[i] == entry {
			u.RootEntries = append(u.RootEntries[:i], u.RootEntries[i+1:]...)
			break
		}
	}
}

func Write(client *septet.Client, entry septet.Entry, stream io.ReadWriter) septet.Reply {
	var data = make([]byte, ReadCount)
	var buf []byte
	var pos = 0
	for {
		i, err := stream.Read(data)
		if err != nil {
			if err == io.EOF {
				break
			} else if err == septet.ErrTimedOut {
				return septet.Reply{451, client.Replies().TimedOut}
			} else {
				return septet.Reply{451, client.Replies().TransmissionError}
			}
		}
		buf = append(buf, data[:i]...)
		pos += i
	}
	entry.(*septet.File).Data = buf
	entry.(*septet.File).Size = int64(len(buf))
	return septet.Reply{226, client.Replies().ClosingData}
}

func OnWrite(client *septet.Client, entry septet.Entry) septet.Reply {
	entry.(*septet.File).Modified = time.Now()
	return client.OpenData(Write, entry)
}

func OnRead(client *septet.Client, entry septet.Entry) septet.Reply {
	return client.OpenData(func (client *septet.Client, entry septet.Entry, stream io.ReadWriter) septet.Reply {
		buf := entry.(*septet.File).Data.([]byte)
		_, err := stream.Write(buf)
		if err != nil {
			return septet.Reply{451, client.Replies().TransmissionError}
		}
		return septet.Reply{226, client.Replies().ClosingData}
	}, entry)
}

func OnAppend(client *septet.Client, entry septet.Entry) septet.Reply {
	return client.OpenData(func (client *septet.Client, entry septet.Entry, stream io.ReadWriter) septet.Reply {
		var data = make([]byte, ReadCount)
		var buf = entry.(*septet.File).Data.([]byte)
		var pos = 0
		for {
			i, err := stream.Read(data)
			if err != nil {
				if err == io.EOF {
					break
				} else if err == septet.ErrTimedOut {
					return septet.Reply{451, client.Replies().TimedOut}
				} else {
					return septet.Reply{451, client.Replies().TransmissionError}
				}
			}
			buf = append(buf, data[:i]...)
			pos += i
		}
		entry.(*septet.File).Size = int64(len(buf))
		return septet.Reply{226, client.Replies().ClosingData}
	}, entry)
}

func OnCreate(client *septet.Client, entry septet.Entry, name string) septet.Reply {
	file, _ := client.NewFile(name)
	file.Append = OnAppend
	file.Delete = OnDelete
	file.Rename = OnRename
	file.Write = OnWrite
	file.Read = OnRead
	file.Created = time.Now()
	file.Modified = file.Created
	file.Size = 0
	entry.(*septet.Directory).Add(file)
	if entry.ID() == 0 {
		client.Data.(*User).AddEntry(file)
	}
	return client.OpenData(Write, file)
}

func OnDelete(client *septet.Client, entry septet.Entry) septet.Reply {
	if entry.Directory() {
		for _, e := range entry.(*septet.Directory).ListEntries() {
			OnDelete(client, e)
		}
	}
	if entry.Parent().ID() == 0 {
		client.Data.(*User).RemoveEntry(entry)
	}
	entry.Parent().Remove(entry)
	return septet.Reply{250, client.Replies().Complete}
}

func OnAccess(client *septet.Client, entry septet.Entry) septet.Reply {
	return septet.Reply{200, client.Replies().Complete}
}

func OnRename(client *septet.Client, entry septet.Entry, newName string) septet.Reply {
	dir := client.LocateParent(newName)
	if dir == nil {
		return septet.Reply{553, client.Replies().NotFound}
	}
	name := septet.PathHead(newName)
	err := dir.MoveName(entry, name)
	if entry.Parent().ID() == 0 {
		client.Data.(*User).RemoveEntry(entry)
	}
	if dir.ID() == 0 {
		client.Data.(*User).AddEntry(entry)
	}
	if err != nil {
		return septet.Reply{553, client.Replies().Exists}
	}
	return septet.Reply{250, client.Replies().Complete}
}

func OnList(client *septet.Client, entry septet.Entry) septet.Reply {
	return client.OpenData(func (client *septet.Client, entry septet.Entry, stream io.ReadWriter) septet.Reply {
		var modify string
		var create string
		var size string
		for _, e := range entry.(*septet.Directory).ListEntries() {
			if e.Directory() {
				modify = e.(*septet.Directory).Modified.UTC().Format("2006-01-02 15:04")
				create = e.(*septet.Directory).Created.UTC().Format("2006-01-02 15:04")
				size = "-"
			} else {
				modify = e.(*septet.File).Modified.UTC().Format("2006-01-02 15:04")
				create = e.(*septet.File).Created.UTC().Format("2006-01-02 15:04")
				size = strconv.FormatInt(e.(*septet.File).Size, 10)
			}
			stream.Write([]byte(fmt.Sprintf("%11s  %s  %s %s\r\n", size, modify, create, e.Name())))
		}
		return septet.Reply{226, client.Replies().ClosingData}
	}, entry)
}

func OnMakeDir(client *septet.Client, entry septet.Entry, name string) septet.Reply {
	dir, _ := client.NewDirectory(name)
	dir.Create = OnCreate
	dir.Delete = OnDelete
	dir.Access = OnAccess
	dir.Rename = OnRename
	dir.List = OnList
	dir.MakeDir = OnMakeDir
	dir.Created = time.Now()
	dir.Modified = dir.Created
	entry.(*septet.Directory).Add(dir)
	if entry.ID() == 0 {
		client.Data.(*User).AddEntry(dir)
	}
	return septet.Reply{257, client.Replies().Complete}
}

func main() {
	septet.SetLoginCallback(func (client *septet.Client, user string, pass string) bool {
		u, ok := Users[user]
		if ok {
			if u.Password != pass {
				return false
			}

			client.Data = u
			for _, e := range u.RootEntries {
				entry, _ := client.RegisterEntry(e)
				client.RootDirectory().Add(entry)
			}
		} else {
			Users[user] = new(User)
			Users[user].Password = pass
			client.Data = Users[user]
		}

		client.RootDirectory().Create = OnCreate
		client.RootDirectory().Access = OnAccess
		client.RootDirectory().List = OnList
		client.RootDirectory().MakeDir = OnMakeDir
		return true
	})
	err := septet.StartServer(septet.Config{
		Timeout: time.Second * 90,
		DataTimeout: time.Second * 30,
		Port: 2021,
		PassiveMode: true,
		PassiveAddress: net.IPv4(127, 0, 0, 1).To4(),
		PassivePorts: nil,
	})
	if err != nil {
		panic(err.Error())
	}
}
