
package main

import (
	"gitlab.com/Hanicef/septet"
	"io"
	"strings"
	"time"
	"net"
	"fmt"
)

type Search struct {
	Search string
	Page int
}

const PageSize = 20
const WelcomeString = "Welcome to the Search example!\r\n" +
	"Try sending SITE FIND <search> to search for <search>.\r\n"

var Entries = map[string][]byte{
	"lorem": []byte("Lorem ipsum dolor sit amet\r\n"),
	"ipsum": []byte("consectetur adipiscing elit. Nulla pellentesque.\r\n"),
	"foo": []byte("Sample text\r\n"),
	"bar": []byte("Insert text here\r\n"),
	"baz": []byte("...not sure what to write here\r\n"),
	"infinite": []byte("Be careful with floating point\r\n"),
	"null": []byte("\r\n"),
}

var PrevDirectory = septet.Directory{}
var NextDirectory = septet.Directory{}

var WelcomeFile = septet.File{
	Size: int64(len(WelcomeString)),
	Read: func (client *septet.Client, entry septet.Entry) septet.Reply {
		return client.OpenData(SendWelcomeData, nil)
	},
}


func SendWelcomeData(client *septet.Client, entry septet.Entry, stream io.ReadWriter) septet.Reply {
	_, err := stream.Write([]byte(WelcomeString))
	if err != nil {
		return septet.Reply{451, client.Replies().TransmissionError}
	}
	return septet.Reply{226, client.Replies().ClosingData}
}

func OnList(client *septet.Client, entry septet.Entry) septet.Reply {
	if client.Data == nil {
		return client.OpenData(SendWelcomeData, nil)
	}
	return client.OpenData(func (client *septet.Client, entry septet.Entry, stream io.ReadWriter) septet.Reply {
		for _, e := range client.RootDirectory().ListEntries() {
			_, err := stream.Write(append([]byte(e.Name()), '\r', '\n'))
			if err != nil {
				return septet.Reply{451, client.Replies().TransmissionError}
			}
		}
		return septet.Reply{226, client.Replies().ClosingData}
	}, nil)
}

func SearchEntries(client *septet.Client, search *Search) {
	var matches int
	client.RootDirectory().Clear()
	if search.Page > 0 {
		entry, err := client.RegisterEntry(&PrevDirectory)
		if err != nil {
			septet.StopServer(err)
		}
		err = client.RootDirectory().Add(entry)
		if err != nil {
			septet.StopServer(err)
		}
	}
	for k, v := range Entries {
		if strings.Contains(k, search.Search) {
			if matches >= PageSize * (search.Page + 1) {
				entry, err := client.RegisterEntry(&NextDirectory)
				if err != nil {
					septet.StopServer(err)
				}
				err = client.RootDirectory().Add(entry)
				if err != nil {
					septet.StopServer(err)
				}
				break
			}
			if matches >= PageSize * search.Page {
				file, err := client.NewFile(k)
				if err != nil {
					septet.StopServer(err)
				}
				file.Data = v
				file.Read = func (client *septet.Client, entry septet.Entry) septet.Reply {
					return client.OpenData(func (client *septet.Client, entry septet.Entry, stream io.ReadWriter) septet.Reply {
						_, err := stream.Write(entry.(*septet.File).Data.([]byte))
						if err != nil {
							return septet.Reply{451, client.Replies().TransmissionError}
						}
						return septet.Reply{226, client.Replies().ClosingData}
					}, entry)
				}
				file.Size = int64(len(v))
				file.Language = "en-US"
				file.MediaType = "text/plain"
				file.Charset = "utf8"
				err = client.RootDirectory().Add(file)
				if err != nil {
					septet.StopServer(err)
				}
			}
			matches++
		}
	}
	if matches == 0 {
		file, err := client.NewFile("No matches")
		if err != nil {
			septet.StopServer(err)
		}
		err = client.RootDirectory().Add(file)
		if err != nil {
			septet.StopServer(err)
		}
	}
}

func main() {
	WelcomeFile.SetName("Welcome!")
	PrevDirectory.SetName("Previous...")
	PrevDirectory.Access = func (client *septet.Client, entry septet.Entry) septet.Reply {
		search := client.Data.(*Search)
		search.Page--
		SearchEntries(client, search)
		client.Relocate("/")
		return septet.Reply{200, client.Replies().Complete}
	}

	NextDirectory.SetName("Next...")
	NextDirectory.Access = func (client *septet.Client, entry septet.Entry) septet.Reply {
		search := client.Data.(*Search)
		search.Page++
		SearchEntries(client, search)
		client.Relocate("/")
		return septet.Reply{200, client.Replies().Complete}
	}

	septet.SetSiteCommand("FIND", func (client *septet.Client, arg string) septet.Reply {
		if arg == "" {
			return septet.Reply{501, client.Replies().NoArgument}
		}

		search := new(Search)
		search.Search = arg
		search.Page = 0
		SearchEntries(client, search)
		client.Data = search
		return septet.Reply{200, client.Replies().Complete}
	})
	septet.SetLoginCallback(func (client *septet.Client, user string, pass string) bool {
		entry, _ := client.RegisterEntry(&WelcomeFile)
		client.RootDirectory().Add(entry)
		client.RootDirectory().List = OnList
		client.RootDirectory().Access = func (client *septet.Client, entry septet.Entry) septet.Reply {
			return septet.Reply{200, client.Replies().Complete}
		}
		return true
	})
	err := septet.StartServer(septet.Config{
		Timeout: time.Second * 120,
		DataTimeout: time.Second * 30,
		Port: 2021,
		PassiveMode: true,
		PassiveAddress: net.IPv4(127,0,0,1).To4(),
		PassivePorts: nil,
	})
	if err != nil {
		fmt.Printf("error: %s\n", err.Error())
	}
}
