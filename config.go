
package septet

import (
	"time"
	"crypto/tls"
	"errors"
	"net"
)

type Config struct {
	// Timeout determines the time until a client times out and their
	// connection is closed. If this is set to 0, clients will never time out.
	Timeout time.Duration

	// DataTimeout determines the time until a client times out with their data
	// connection, including connecting to the data socket. If this is set to
	// 0, clients will never time out.
	DataTimeout time.Duration

	// Port defines what port to listen on. The standard port is 20, which is 
	// the port that most clients connect to if the user haven't explicitly
	// entered a different port.
	Port uint16

	// PassiveMode determines if passive mode is enabled or not. Passive mode
	// allows clients behind a NAT or firewall to connect their data port, and
	// is highly recommended to enable.
	PassiveMode bool

	// PassiveAddress defines what IPv4 address to connect to when non-extended
	// passive mode in requested by the client. Setting this incorrectly
	// results in IPv4 users not being able to connect the data connection
	// properly.
	PassiveAddress net.IP

	// PassivePorts defines what ports to use for listening to data connections
	// on, if PassiveMode is true. Setting this to nil indicates all available
	// ports on the system may be used.
	PassivePorts []uint16

	// Languages is a map of languages that is supported on the server. The map
	// contains a list of reply messages to be used based on what language a
	// a client is using. Setting this to nil will disable localization and the
	// default reply messages will be used instead.
	Languages map[string]*ReplyMessages

	// DefaultLanguage is the default language to set for every new client that
	// connects. If Languages is nil, this field is ignored. The server will
	// fail with an error if DefaultLanguage is not located within Languages.
	DefaultLanguage string

	// CertFile is a string that determines which TLS certificate to use for
	// TLS. If either CertFile or KeyFile is an empty string, TLS will be
	// disabled.
	CertFile string

	// KeyFile is a string that determines which TLS private key to use for
	// TLS. If either CertFile or KeyFile is an empty string, TLS will be
	// disabled.
	KeyFile string

	// ForceTLS indicates whenever TLS is required to log in or not. If this is
	// set to true, all login attempts will be rejected unless the client
	// establishes a TLS session beforehand. This is useful if your server is
	// transferring sensitive information such as passwords.
	ForceTLS bool
}

var conf Config

var cert tls.Certificate
var tlsConfig tls.Config

func initConfig(config *Config) error {
	if config.PassiveMode {
		if len(config.PassiveAddress) != net.IPv4len {
			return errors.New("PassiveAddress is not an IPv4 address.")
		}
		if config.PassivePorts != nil && len(config.PassivePorts) == 0 {
			return errors.New("PassivePorts is empty")
		}
	}
	if config.Languages != nil {
		_, ok := config.Languages[config.DefaultLanguage]
		if !ok {
			return errors.New("DefaultLanguage does not exist within Languages")
		}
	}

	if config.CertFile != "" && config.KeyFile != "" {
		var err error
		cert, err = tls.LoadX509KeyPair(config.CertFile, config.KeyFile)
		if err != nil {
			return errors.New("TLS error: " + err.Error())
		}

		tlsConfig.Certificates = []tls.Certificate{cert}
	} else {
		cert.Certificate = nil
	}

	return nil
}
