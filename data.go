
package septet

import (
	"strings"
	"fmt"
	"net"
	"io"
	"crypto/tls"
	"time"
	"bytes"
	"strconv"
)

type dataStream struct {
	client *Client
	open bool
	size int64
}

func processPort(client *Client, data string) error {
	var err error
	if client.data != nil {
		return sendReply(client, 500, client.Replies().NotPossible)
	} else if data == "" {
		return sendReply(client, 501, client.Replies().NoArgument)
	}

	parts := strings.Split(data, ",")
	if len(parts) < 6 {
		return sendReply(client, 501, client.Replies().TooFewFields)
	} else if len(parts) > 6 {
		return sendReply(client, 501, client.Replies().TooManyFields)
	}

	var ip []byte = make([]byte, 4)
	for i := 0; i < 4; i++ {
		// Just make sure all fields fits into 8 bits; this is only for validation.
		val, err := strconv.ParseUint(parts[i], 10, 8)
		if err != nil {
			return sendReply(client, 501, client.Replies().BadField)
		}
		ip[i] = byte(val)
	}

	high, err := strconv.ParseUint(parts[4], 10, 8)
	if err != nil {
		return sendReply(client, 501, client.Replies().BadField)
	}

	low, err := strconv.ParseUint(parts[5], 10, 8)
	if err != nil {
		return sendReply(client, 501, client.Replies().BadField)
	}

	closePassive(client)
	client.dataAddr = &net.TCPAddr{ip[:4], int((high << 8) | low), ""}
	return sendReply(client, 200, client.Replies().Complete)
}

func processPasv(client *Client, data string) error {
	if !conf.PassiveMode {
		return sendReply(client, 502, client.Replies().NotSupported)
	} else if client.data != nil {
		return sendReply(client, 500, client.Replies().NotPossible)
	} else {
		local := conf.PassiveAddress
		err := initPassive(client)
		if err != nil {
			return sendReply(client, 421, client.Replies().PassiveError)
		}
		pasvPort := client.dataListen.Addr().(*net.TCPAddr).Port
		return sendReply(client, 227, client.Replies().Passive, fmt.Sprintf(" (%d,%d,%d,%d,%d,%d)", local[0], local[1], local[2], local[3], pasvPort >> 8, pasvPort & 0xff))
	}
}

func processEprt(client *Client, data string) error {
	var err error
	if client.data != nil {
		return sendReply(client, 500, client.Replies().NotPossible)
	} else if data == "" {
		return sendReply(client, 501, client.Replies().NoArgument)
	}

	if data[0] != '|' || data[len(data)-1] != '|' {
		return sendReply(client, 501, client.Replies().MissingDelimeters)
	}
	parts := strings.Split(data[1:len(data)-1], "|")
	if len(parts) < 3 {
		return sendReply(client, 501, client.Replies().TooFewFields)
	} else if len(parts) > 3 {
		return sendReply(client, 501, client.Replies().TooManyFields)
	}

	if parts[0][0] != '1' && parts[0][0] != '2' {
		return sendReply(client, 501, client.Replies().BadProtocol)
	}

	addr := net.ParseIP(parts[1])
	if addr == nil {
		return sendReply(client, 501, client.Replies().BadAddress)
	}

	port, err := strconv.ParseUint(parts[2], 10, 16)
	if err != nil {
		return sendReply(client, 501, client.Replies().BadPort)
	}

	closePassive(client)
	client.dataAddr = &net.TCPAddr{addr, int(port), ""}
	return sendReply(client, 200, client.Replies().Complete)
}

func processEpsv(client *Client, data string) error {
	if !conf.PassiveMode {
		return sendReply(client, 502, client.Replies().NotSupported)
	} else if client.data != nil {
		return sendReply(client, 500, client.Replies().NotPossible)
	} else {
		err := initPassive(client)
		if err != nil {
			return sendReply(client, 421, client.Replies().PassiveError)
		}
		pasvPort := client.dataListen.Addr().(*net.TCPAddr).Port
		return sendReply(client, 229, client.Replies().Passive, " (|||", pasvPort, "|)")
	}
}

// OpenData opens the data port and dispatches callback if the open was
// successful. It returns a reply which should be returned from the calling
// event to inform the client to establish a data connection to the server.
//
// The second parameter is passed onto the callback function. This is used to
// process data on the file or directory in question, which allows reusing
// callback functions for different events.
func (c *Client) OpenData(callback func (*Client, Entry, io.ReadWriter) Reply, entry Entry) Reply {
	c.dataCallback = callback
	c.dataEntry = entry

	if c.data != nil {
		return Reply{125, c.Replies().DataOpen}
	}
	return Reply{150, c.Replies().OpeningData}
}

func dispatchData(client *Client, size int64) error {
	var err error
	if client.data != nil {
		err = sendReply(client, 125, client.Replies().DataOpen)
		if err != nil {
			return err
		}
	} else {
		err = sendReply(client, 150, client.Replies().OpeningData)
		if err != nil {
			return err
		}

		if client.dataListen != nil {
			waitForPassive(client)
			if client.data == nil {
				return sendReply(client, 425, client.Replies().TimedOut)
			}
		} else {
			client.data, err = net.DialTimeout("tcp", client.dataAddr.String(), conf.DataTimeout)
			if err != nil {
				if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
					return sendReply(client, 425, client.Replies().TimedOut)
				}
				return sendReply(client, 425, client.Replies().CannotOpenData)
			}
		}

		if client.tlsData {
			conn := tls.Server(client.data, &tlsConfig)
			err = conn.Handshake()
			if err != nil {
				client.data.Close()
				client.data = nil
				return err
			}
			client.data = conn
		}
	}
	go func() {
		stream := client.createDataStream(size)
		reply := client.dataCallback(client, client.dataEntry, &stream)
		if reply.Code != 250 {
			client.closeData()
		}
		client.dataCallback = nil
		if client.control != nil {
			sendReply(client, reply.Code, reply.Message)
		}
	}()
	return nil
}

func (client *Client) closeData() {
	if client.record {
		client.data.Write([]byte{0xff, 0x03})
	}
	client.data.Close()
	client.data = nil
}

func (client *Client) createDataStream(size int64) (data dataStream) {
	data.client = client
	data.open = false
	// size indicates skip amount on retrieve.
	data.size = size
	return
}

func (data *dataStream) Read(p []byte) (int, error) {
	var err error
	if !data.client.isOpen() || (data.open && data.client.data == nil) {
		return 0, ErrClosed
	}

	if data.size == 0 {
		return 0, io.EOF
	} else if data.size > 0 && int64(len(p)) > data.size {
		p = p[:data.size]
	}

	err = data.client.data.SetDeadline(time.Now().Add(conf.DataTimeout))
	if err != nil {
		return 0, err
	}

	size, err := data.client.data.Read(p)
	if err != nil && data.client.record {
		for i := 0; i < size; i++ {
			if p[i] == 0xff {
				if p[i+1] & ^byte(1) == 2 {
					// EOF
					p = p[:i]
					size = i
					data.size = 0
					break
				} else if p[i+1] & 1 != 0 {
					// Ignore EOR; they aren't that important anyways.
					size--
					for j := i; j < size; j++ {
						p[j] = p[j+1]
					}
				}
			}
		}
	} else if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
		return 0, ErrTimedOut
	}
	data.size -= int64(size)
	return size, err
}

func (data *dataStream) Write(p []byte) (int, error) {
	var err error
	if !data.client.isOpen() || (data.open && data.client.data == nil) {
		return 0, ErrClosed
	}

	if data.size >= int64(len(p)) {
		data.size -= int64(len(p))
		return len(p), nil
	} else if data.size > 0 {
		p = p[data.size:]
	}

	err = data.client.data.SetDeadline(time.Now().Add(conf.DataTimeout))
	if err != nil {
		return 0, err
	}

	if data.client.record {
		p = bytes.ReplaceAll(p, []byte{0xff}, []byte{0xff, 0xff})
	}

	size, err := data.client.data.Write(p)
	size += int(data.size)
	data.size = 0
	return size, err
}
