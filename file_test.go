
package septet

import (
	"os"
	"testing"
)

var root *Directory
var client = Client{}

var foo *Directory
var bar *Directory
var baz *Directory
var loren *File
var ipsum *File
var entry Entry

func TestMain(m *testing.M) {
	client.RootDirectory().client = &client

	foo, _ = client.NewDirectory("foo")
	loren, _ = client.NewFile("loren")
	client.RootDirectory().Add(foo, loren)

	bar, _ = client.NewDirectory("bar")
	baz, _ = client.NewDirectory("baz")
	ipsum, _ = client.NewFile("ipsum")
	entry, _ = client.RegisterEntry(foo)
	bar.Add(baz, ipsum, entry)
	client.RootDirectory().Add(bar)

	root = client.RootDirectory()
	os.Exit(m.Run())
}

func TestGet(t *testing.T) {
	bar := root.Get("bar")
	if bar == nil || bar.Name() != "bar" {
		t.Error("Get(bar) failed!")
		return
	}

	ipsum := bar.(*Directory).Get("ipsum")
	if ipsum == nil || ipsum.Name() != "ipsum" {
		t.Error("Get(ipsum) failed!")
		return
	}

	lipsum := root.Find("ipsum")
	if lipsum == nil || lipsum[0].Name() != "ipsum" {
		t.Error("Find(ipsum) failed!")
		return
	}
}

func TestPath(t *testing.T) {
	ipsum := root.Get("bar").(*Directory).Get("ipsum").(*File)
	if ipsum.Path() != "/bar/ipsum" {
		t.Error("Incorrect path; expected /bar/ipsum but got", ipsum.Path())
	}

	if root.Path() != "/" {
		t.Error("Incorrect path; expected / but got", root.Path())
	}
}

func TestRegister(t *testing.T) {
	dir, _ := client.NewDirectory("foo")
	entry, _ := client.RegisterEntry(dir)
	dir = entry.(*Directory)
	if dir.ID() == 0 {
		t.Error("ID failed to register")
	}

	if dir.Name() != "foo" {
		t.Error("Name failed to register; expected foo but got", dir.Name())
	}

	if dir.Client() != &client {
		t.Error("Client failed to register")
	}
}

func TestLocate(t *testing.T) {
	if client.Locate("/foo") != foo {
		t.Error("Locate('/foo') failed!")
	}
	if client.Locate("/bar/ipsum") != ipsum {
		t.Error("Locate('/bar/ipsum') failed!")
	}
	if client.Locate("/baz") != nil {
		t.Error("Locate('/baz') failed!")
	}
	if client.Locate("/foo/bar/baz") != nil {
		t.Error("Locate('/foo/bar/baz') failed!")
	}
	if client.Locate("/foo/bar/baz/") != nil {
		t.Error("Locate('/foo/bar/baz/') failed!")
	}
	client.current = client.root.Get("bar").(*Directory)
	if client.Locate("ipsum") != ipsum {
		t.Error("Locate('ipsum') failed!")
	}
	if client.Locate("baz/") != baz {
		t.Error("Locate('baz/') failed!")
	}
	if client.Locate("loren") != nil {
		t.Error("Locate('loren') failed!")
	}
	if client.Locate("/") != client.RootDirectory() {
		t.Error("Locate('/') failed!")
	}

	if client.LocateParent("/foo") != client.Locate("/") {
		t.Error("LocateParent('/foo') failed!")
	}
	if client.LocateParent("/bar/ipsum") != client.Locate("/bar") {
		t.Error("LocateParent('/bar/ipsum') failed!")
	}
	if client.LocateParent("ipsum") != client.Locate("/bar") {
		t.Error("LocateParent('ipsum') failed!")
	}
	if client.LocateParent("baz/file") != client.Locate("/bar/baz") {
		t.Error("LocateParent('baz/file') failed!")
	}
	if client.LocateParent("/") != client.Locate("/") {
		t.Error("LocateParent('/') failed!")
	}
}

func TestMove(t *testing.T) {
	var client = Client{}
	client.RootDirectory().client = &client
	client.current = &client.root

	foo, _ := client.NewDirectory("foo")
	loren, _ := client.NewFile("loren")
	client.RootDirectory().Add(foo, loren)

	bar, _ := client.NewDirectory("bar")
	baz, _ := client.NewDirectory("baz")
	ipsum, _ := client.NewFile("ipsum")
	entry, _ := client.RegisterEntry(foo)
	bar.Add(baz, ipsum, entry)
	client.RootDirectory().Add(bar)
	if client.RootDirectory().Move(client.Locate("/bar/baz")) != nil {
		t.Error("Move('/bar/baz') failed!")
	}
	if client.RootDirectory().Move(client.Locate("/bar/foo")) == nil {
		t.Error("Move('/bar/foo') failed!")
	}
	err := client.RootDirectory().MoveName(client.Locate("/bar/foo"), "ipsum")
	if err != nil {
		t.Error("MoveName('/bar/foo', 'ipsum') failed! Error:", err.Error())
	}
	if client.RootDirectory().Move(nil) == nil {
		t.Error("Move(nil) failed!")
	}
	if client.Locate("/foo").(*Directory).Move(client.RootDirectory()) == nil {
		t.Error("Move('/') failed!")
	}
}
