
package septet

import (
	"strconv"
	"reflect"
	"time"
)

// ReplyMessages contains values that are returned as reply messages in
// internal events.
type ReplyMessages struct {
	// Ready is sent when the client is ready to be served.
	Ready string
	// Bye is sent when the client has disconnected from the server.
	Bye string
	// LineTooLong is sent when a command line is too long to handle.
	LineTooLong string
	// TimedOut is sent when a client has timed out.
	TimedOut string
	// ConnectionError is sent when a unknown connection error has occured.
	ConnectionError string

	// Complete is sent when a command has completed successfully.
	Complete string
	// Proceed is sent when the client may proceed with an operation.
	Proceed string
	// NotSupported is sent when a command or operation is recognized but not supported.
	NotSupported string
	// NoArgument is sent when no argument is recieved where one is expected.
	NoArgument string
	// UnknownCommand is sent when an unknown command is recieved.
	UnknownCommand string
	// Passive is sent when a client has entered passive mode.
	Passive string
	// PassiveError is sent when an error occurs during initialization of passive mode.
	PassiveError string

	// LoggedIn is sent when a client is logged in successfully.
	LoggedIn string
	// LoginFailed is sent when a log-in attempt failed.
	LoginFailed string
	// InvalidUsername is sent when a username is invalid.
	InvalidUsername string
	// NeedPassword is sent when a username is valid but a password is required.
	NeedPassword string
	// NotLoggedIn is sent when a client attepmts an operation that required being logged in.
	NotLoggedIn string
	// NoPrivileges is sent when a client has no privileges to perform an operation.
	NoPrivileges string

	// TooFewFields is sent when an insufficent amount of fields are retrieved in PORT or EPRT.
	TooFewFields string
	// TooManyFields is sent when too many fields are retrieved in PORT or EPRT.
	TooManyFields string
	// BadField is sent when one or more fields in PORT or EPRT is invalid.
	BadField string
	// BadProcotol is sent when the PORT or EPRT protocol is invalid.
	BadProtocol string
	// BadAddress is sent when the PORT or EPRT address is invalid.
	BadAddress string
	// BadPort is sent when the PORT or EPRT port is invalid.
	BadPort string
	// MissingDelimeters is sent when the surrounding delimeters in EPRT are missing.
	MissingDelimeters string

	// NotFound is sent when an entry is not found.
	NotFound string
	// Exists is sent when an entry already exists.
	Exists string
	// NotADirectory is sent when an entry is not a directory where one is expected.
	NotADirectory string
	// IsADirectory is sent when an entry is a directory where one is not expected.
	IsADirectory string
	// AtRootDirectory is sent when a client tries to navigate beyond the root directory.
	AtRootDirectory string
	// NoDate is sent when modified date is requested on a file where this is not defined.
	NoDate string
	// NoSize is sent when file size is requested on a file where this is not defined.
	NoSize string

	// OpeningData is sent when the data connection is opening.
	OpeningData string
	// CannotOpenData is sent when the data connection failed to open.
	CannotOpenData string
	// DataOpen is sent when the data connection is already opened.
	DataOpen string
	// ClosingData is sent when the data connection is closing.
	ClosingData string
	// TransmissionError is sent when data transfer failed on the data connection.
	TransmissionError string
	// Restart is sent when a transfer restart is requested.
	Restart string
	// NoRestart is sent when a transfer restart is not possible.
	NoRestart string

	// Encrypting is sent when the client is establishing a TLS connection.
	Encrypting string
	// NoEncryption is sent when the encryption method is unknown.
	NoEncryption string
	// EncryptionFailed is sent when an encryption establisment fails.
	EncryptionFailed string
	// NotEncrypted is sent when an encryption command is sent on a non-encrypted connection.
	NotEncrypted string
	// NoProtect is sent when an invalid data protection level in recieved.
	NoProtect string
	// TLSRequired is sent when a client attempts to log in without TLS when TLS is required.
	TLSRequired string

	// NeedAllo is sent when a data store requested when the amount of data is not specified.
	NeedAllo string
	// NotANumber is sent when a anything but a number is sent where one in expected.
	NotANumber string
	// BadSequence is sent when a command chain is sent in the wrong order.
	BadSequence string
	// Features is sent at the beginning of the feature list.
	Features string
	// List is sent at the beginning of a machine listing.
	List string
	// End is sent at the end of a list.
	End string
	// NoLanguage is sent when the client requests an unsupported language.
	NoLanguage string
	// UnknownOption is sent when an unknown OPTS parameter is sent.
	UnknownOption string
	// NotPossible is sent when an inappropriate command is sent during data transmission.
	NotPossible string
}

// DefaultReplies are the default reply messages, which are used when Languages
// in the configuration is nil. This may be altered before the server has
// started to better fit the server's need, but altering this while the server
// is running is strongly discouraged due to the high risks of race conditions.
var DefaultReplies = ReplyMessages{
	Ready: "Ready.",
	Bye: "Bye.",
	LineTooLong: "Line too long.",
	TimedOut: "Timed out.",
	ConnectionError: "Connection error.",

	Complete: "Complete.",
	Proceed: "Proceed.",
	NotSupported: "Not supported.",
	NoArgument: "Argument missing.",
	UnknownCommand: "Unknown command.",
	Passive: "Passive.",
	PassiveError: "Passive error.",

	LoggedIn: "Logged in.",
	LoginFailed: "Login failed.",
	InvalidUsername: "Invalid username.",
	NeedPassword: "Need password.",
	NotLoggedIn: "Not logged in.",
	NoPrivileges: "Insufficent privileges.",

	TooFewFields: "Not enough fields.",
	TooManyFields: "Too many fields.",
	BadField: "Malformatted field(s).",
	BadProtocol: "Unknown protocol.",
	BadAddress: "Bad address format.",
	BadPort: "Bad port.",
	MissingDelimeters: "Missing surrounding delimeters.",

	NotFound: "Not found.",
	Exists: "Exists.",
	NotADirectory: "Not a directory.",
	IsADirectory: "Is a directory.",
	AtRootDirectory: "At root directory.",
	NoDate: "No date.",
	NoSize: "Size unknown.",

	OpeningData: "Opening data.",
	CannotOpenData: "Cannot open data.",
	DataOpen: "Data open.",
	ClosingData: "Closing data.",
	TransmissionError: "Transmission error.",
	Restart: "Restart scheduled.",
	NoRestart: "Unable to restart.",

	Encrypting: "Begin encryption.",
	NoEncryption: "Unknown method.",
	EncryptionFailed: "Encryption failed.",
	NotEncrypted: "Not encrypted.",
	NoProtect: "Unknown protection.",
	TLSRequired: "TLS required.",

	NeedAllo: "Need ALLO.",
	NotANumber: "Not a number.",
	BadSequence: "Bad sequence.",
	Features: "Features.",
	List: "Listing.",
	End: "End.",
	NoLanguage: "No such language.",
	UnknownOption: "Unknown option.",
	NotPossible: "Not currently possible.",
}

// Reply contains reply data, which is sent to the client after an operation.
type Reply struct {
	Code int
	Message string
}

const floatAccuracy = 6

// Replies returns a pointer to the ReplyMessages structure for the language
// the client is currently using. While the server is running, using this
// function is recommended over accessing DefaultReplies directly, as this
// function handles localization automatically and makes implementing new
// languages easier.
func (c *Client) Replies() *ReplyMessages {
	if conf.Languages == nil {
		return &DefaultReplies
	}

	return conf.Languages[c.lang]
}

func sendReply(client *Client, reply int, args... interface{}) error {
	var data []byte
	var size = 4

	var uintLength = func (i uint) int {
		var size int
		for size = 0; i > 0; size++ {
			i /= 10
		}
		return size
	}
	var intLength = func (i int) int {
		if i < 0 {
			return uintLength(uint(-i)) + 1
		} else {
			return uintLength(uint(i))
		}
	}
	var floatLength = func (f float64) int {
		var size = 0
		if f < 0 {
			f = -f
			size++
		}
		for ; f >= 1.0; size++ {
			f /= 10.0
		}
		return size + floatAccuracy
	}

	if reply < 100 || reply > 999 {
		panic("unhandlable reply code " + strconv.FormatInt(int64(reply), 10))
	}

	for _, arg := range args {
		k := reflect.TypeOf(arg).Kind()
		switch k {
		case reflect.Bool:
			if arg.(bool) {
				size += 4
			} else {
				size += 5
			}
		case reflect.Int:
			size += intLength(arg.(int))
		case reflect.Uint:
			size += uintLength(arg.(uint))
		case reflect.Uint16:
			size += uintLength(uint(arg.(uint16)))
		case reflect.Float32:
			size += floatLength(float64(arg.(float32)))
		case reflect.Float64:
			size += floatLength(arg.(float64))
		case reflect.String:
			size += len(arg.(string))
		default:
			panic("cant format type " + k.String())
		}
	}

	data = make([]byte, 4, size + 2)
	for i := 2; reply != 0; i-- {
		data[i] = byte((reply % 10) + '0')
		reply /= 10
	}
	data[3] = ' '

	for _, arg := range args {
		k := reflect.TypeOf(arg).Kind()
		switch k {
		case reflect.Bool:
			data = append(data, []byte(strconv.FormatBool(arg.(bool)))...)
		case reflect.Int:
			data = append(data, []byte(strconv.FormatInt(int64(arg.(int)), 10))...)
		case reflect.Uint:
			data = append(data, []byte(strconv.FormatUint(uint64(arg.(uint)), 10))...)
		case reflect.Uint16:
			data = append(data, []byte(strconv.FormatUint(uint64(arg.(uint16)), 10))...)
		case reflect.Float32:
			data = append(data, []byte(strconv.FormatFloat(float64(arg.(float32)), 'G', floatAccuracy, 32))...)
		case reflect.Float64:
			data = append(data, []byte(strconv.FormatFloat(arg.(float64), 'G', floatAccuracy, 32))...)
		case reflect.String:
			data = append(data, []byte(arg.(string))...)
		}
	}

	data = append(data, '\r', '\n')
	err := client.control.SetWriteDeadline(time.Now().Add(conf.Timeout))
	if err == nil {
		_, err = client.control.Write(data)
	}
	return err
}
