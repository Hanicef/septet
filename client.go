
package septet

import (
	"net"
	"errors"
	"io"
)

// Client represents a client and it's corresponding data.
type Client struct {
	slot int
	idCounter int

	tls bool
	control net.Conn

	tlsData bool
	dataListen net.Listener
	dataBuffer net.Conn
	dataEntry Entry
	data net.Conn
	dataCallback func (*Client, Entry, io.ReadWriter) Reply
	dataAddr *net.TCPAddr

	loggedIn bool
	root Directory
	current *Directory

	lang string
	facts int
	record bool
	allo int64

	// Data is a user-defined interface that may be set to anything
	// client-related, and will not be used or altered internally. This is
	// meant to be used per session, and is not recommended to be shared
	// between multiple clients.
	Data interface{}
}

var clients []*Client

var errCloseClient = errors.New("close client")

var loginCallback func (*Client, string, string) bool
var langCallback func (*Client, string)

func newClient() *Client {
	var i int
	for i = 0; i < len(clients); i++ {
		if clients[i] == nil {
			clients[i] = new(Client)
			clients[i].slot = i
			return clients[i]
		}
	}

	clients = append(clients, new(Client))
	clients[i].slot = i
	return clients[i]
}

func closeAllClients() {
	for i := range clients {
		clients[i].Close()
	}
}

// Close disconnect the client from the server, closing all connections and
// deallocates all data of the client.
func (c *Client) Close() {
	if c.control != nil {
		c.control.Close()
		c.control = nil
	}
	if c.data != nil {
		c.data.Close()
		c.data = nil
	}
	if c.dataListen != nil {
		c.dataListen.Close()
		c.dataListen = nil
	}
	if c.dataBuffer != nil {
		c.dataBuffer.Close()
		c.dataBuffer = nil
	}

	// Set the slot to nil so the garbage collector can free the client.
	clients[c.slot] = nil
}

func (c *Client) isOpen() bool {
	return c.control != nil
}

// Encrypted returns true if the client is encrypted using TLS, false otherwise.
func (c *Client) Encrypted() bool {
	return c.tls
}

// DataEncrypted returns true if the client is using TLS over the data
// connection, false otherwise.
func (c *Client) DataEncrypted() bool {
	return c.tlsData
}

// SetLoginCallback sets the callback function whenever a client is attempting
// to log into the service. callback takes three arguments: the client itself,
// the username that the client is trying to log in as, and the password.
// Returning true indicates a successful login; false indicates a failed.
//
// It is recommended that the client tree is initialized in this function. If
// a client has logged in successfully, the client is then allowed to use the
// tree for listing, retrieving, storing, etc. Not having an initialized tree
// at this point will lead to privilege errors on all operations. Don't forget
// to populate the root directory using RootDirectory!
//
// Attempting to set the login callback when the server is running will result
// in a panic.
func SetLoginCallback(callback func (*Client, string, string) bool) {
	if listenSocket != nil {
		panic("attempted to set login callback when server is running")
	}
	loginCallback = callback
}

// Language returns the current language of the client.
func (c *Client) Language() string {
	return c.lang
}

// SetLanguageCallback sets the callback function for whenever the client
// changes the current language. This is useful for adapting the source tree
// to a language, like changing directory names to match the language or
// changing file sizes to match the file size for a specific language.
//
// Note that this function will not be called if a language is not defined. All
// checking is done internally, and the callback will only be called if a
// language is changed successfully. This provides a gurantee that only the
// languages defined within the configuration can be passed into this callback.
//
// A caveat with this callback is that language changes may occur before the
// client has logged in. That means that the callback could be called before
// the client tree is initialized. Therefore, it is important to check if the
// client is initialized by simply fetching files and directories that may, or
// should, be present after the tree is initialized. If the files are not
// present, you can call the callback directly from the login callback with the
// second parameter retrieved from Language, to avoid repeating code.
//
// Attempting to set the language callback when the server is running will
// result in a panic.
func SetLanguageCallback(callback func (*Client, string)) {
	if listenSocket != nil {
		panic("attempted to set language callback when server is running")
	}
	langCallback = callback
}
