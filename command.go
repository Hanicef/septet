
package septet

import (
	"strings"
	"strconv"
	"time"
	"fmt"
	"io"
	"crypto/tls"
)

type command func(*Client, string) error

type commandSeq struct {
	cmd string
	function func(*Client, string, []string) (bool, error)
	next []commandSeq
}

func handleReply(client *Client, reply Reply, allo int64) error {
	if (reply.Code == 125 || reply.Code == 150) && client.dataCallback != nil {
		return dispatchData(client, allo)
	} else if reply.Code == 421 {
		err := sendReply(client, reply.Code, reply.Message)
		client.Close()
		return err
	} else {
		return sendReply(client, reply.Code, reply.Message)
	}
}

var commands map[string]command = map[string]command {
	"QUIT": func (client *Client, arg string) error {
		err := sendReply(client, 221, client.Replies().Bye)
		client.Close()
		return err
	},
	"EPRT": processEprt,
	"EPSV": processEpsv,
	"PASV": processPasv,
	"PORT": processPort,
	"STRU": func (client *Client, arg string) error {
		if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		}
		if arg == "" {
			return sendReply(client, 501, client.Replies().NoArgument)
		} else if arg[0] == 'f' || arg[0] == 'F' {
			client.record = false
			return sendReply(client, 200, client.Replies().Complete)
		} else if arg[0] == 'r' || arg[0] == 'R' {
			client.record = true
			return sendReply(client, 200, client.Replies().Complete)
		} else {
			return sendReply(client, 504, client.Replies().NotSupported)
		}
	},
	"TYPE": func (client *Client, arg string) error {
		if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		}
		if arg == "" {
			return sendReply(client, 501, client.Replies().NoArgument)
		} else if arg[0] == 'a' || arg[0] == 'A' || arg[0] == 'i' || arg[0] == 'I' {
			return sendReply(client, 200, client.Replies().Complete)
		} else {
			return sendReply(client, 504, client.Replies().NotSupported)
		}
	},
	"MODE": func (client *Client, arg string) error {
		if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		}
		if arg == "" {
			return sendReply(client, 501, client.Replies().NoArgument)
		} else if arg[0] == 's' || arg[0] == 'S' {
			return sendReply(client, 200, client.Replies().Complete)
		} else {
			return sendReply(client, 504, client.Replies().NotSupported)
		}
	},
	"PASS": func (client *Client, arg string) error {
		return sendReply(client, 503, client.Replies().BadSequence)
	},
	"PWD": func (client *Client, arg string) error {
		return sendReply(client, 257, "\"", strings.Replace(client.current.Path(), "\"", "\"\"", -1), "\" ", client.Replies().Complete)
	},
	"CWD": func (client *Client, arg string) error {
		if !client.loggedIn {
			return sendReply(client, 530, client.Replies().NotLoggedIn)
		} else if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		}
		entry := client.Locate(arg)
		if entry == nil {
			return sendReply(client, 550, client.Replies().NotFound)
		} else if dir, ok := entry.(*Directory); ok {
			if dir.Access == nil {
				return sendReply(client, 530, client.Replies().NoPrivileges)
			}
			reply := dir.Access(client, dir)
			if reply.Code == 250 || reply.Code == 200 {
				reply.Code = 250
				client.current = dir
			}
			return handleReply(client, reply, 0)
		}
		return sendReply(client, 550, client.Replies().NotADirectory)
	},
	"CDUP": func (client *Client, arg string) error {
		if !client.loggedIn {
			return sendReply(client, 530, client.Replies().NotLoggedIn)
		} else if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		} else if client.current.Parent() == nil {
			return sendReply(client, 550, client.Replies().AtRootDirectory)
		} else if client.current.Parent().Access == nil {
			return sendReply(client, 530, client.Replies().NoPrivileges)
		}
		reply := client.current.Parent().Access(client, client.current.Parent())
		if reply.Code == 250 || reply.Code == 200 {
			reply.Code = 200
			client.current = client.current.Parent()
		}
		return handleReply(client, reply, 0)
	},
	"RETR": func (client *Client, arg string) error {
		if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		}
		return retrieve(client, arg, 0)
	},
	"ALLO": func (client *Client, arg string) error {
		if !client.loggedIn {
			return sendReply(client, 530, client.Replies().NotLoggedIn)
		} else if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		} else if arg == "" {
			client.allo = -1
			return sendReply(client, 200, client.Replies().Complete)
		}
		var err error
		client.allo, err = strconv.ParseInt(arg, 10, 64)
		if err != nil || client.allo < 0 {
			return sendReply(client, 501, client.Replies().NotANumber)
		}
		return sendReply(client, 200, client.Replies().Complete)
	},
	"STOR": func (client *Client, arg string) error {
		if client.allo == -1 && !client.record {
			return sendReply(client, 452, client.Replies().NeedAllo)
		}
		return store(client, arg, false)
	},
	"APPE": func (client *Client, arg string) error {
		if client.allo == -1 && !client.record {
			return sendReply(client, 452, client.Replies().NeedAllo)
		}
		return store(client, arg, true)
	},
	"ABOR": func (client *Client, arg string) error {
		if client.dataCallback != nil {
			client.closeData()
			client.dataCallback = nil
		}
		return sendReply(client, 226, client.Replies().ClosingData)
	},
	"DELE": func (client *Client, arg string) error {
		var reply Reply
		if !client.loggedIn {
			return sendReply(client, 530, client.Replies().NotLoggedIn)
		} else if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		} else if arg == "" {
			return sendReply(client, 501, client.Replies().NoArgument)
		}
		entry := client.Locate(arg)
		if entry == nil {
			return sendReply(client, 550, client.Replies().NotFound)
		}
		if entry.Directory() {
			if entry.(*Directory).Delete == nil {
				return sendReply(client, 530, client.Replies().NoPrivileges)
			}
			reply = entry.(*Directory).Delete(client, entry)
		} else {
			if entry.(*File).Delete == nil {
				return sendReply(client, 530, client.Replies().NoPrivileges)
			}
			reply = entry.(*File).Delete(client, entry)
		}
		return handleReply(client, reply, 0)
	},
	"RNTO": func (client *Client, arg string) error {
		return sendReply(client, 503, client.Replies().BadSequence)
	},
	"MKD": func (client *Client, arg string) error {
		if !client.loggedIn {
			return sendReply(client, 530, client.Replies().NotLoggedIn)
		} else if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		} else if arg == "" {
			return sendReply(client, 501, client.Replies().NoArgument)
		}

		var dir *Directory
		entry, arg := client.splitPath(arg)
		if entry == nil {
			return sendReply(client, 550, client.Replies().NotFound)
		} else if !entry.Directory() {
			return sendReply(client, 550, client.Replies().NotADirectory)
		}
		dir = entry.(*Directory)

		if client.current.MakeDir == nil {
			return sendReply(client, 530, client.Replies().NoPrivileges)
		}

		if dir.Get(arg) != nil {
			return sendReply(client, 550, client.Replies().Exists)
		}
		reply := dir.MakeDir(client, dir, arg)
		if reply.Code == 257 {
			name := strings.Replace(arg, "\"", "\"\"", -1)
			if dir.ID() == 0 {
				return sendReply(client, reply.Code, fmt.Sprintf("\"/%s\" %s", name, reply.Message))
			} else {
				return sendReply(client, reply.Code, fmt.Sprintf("\"%s/%s\" %s", dir.Path(), name, reply.Message))
			}
		} else {
			return handleReply(client, reply, 0)
		}
	},
	"RMD": func (client *Client, arg string) error {
		if !client.loggedIn {
			return sendReply(client, 530, client.Replies().NotLoggedIn)
		} else if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		} else if arg == "" {
			return sendReply(client, 501, client.Replies().NoArgument)
		}
		entry := client.Locate(arg)
		if entry == nil {
			return sendReply(client, 550, client.Replies().NotFound)
		} else if !entry.Directory() {
			return sendReply(client, 550, client.Replies().NotADirectory)
		} else if entry.(*Directory).Delete == nil {
			return sendReply(client, 530, client.Replies().NoPrivileges)
		}
		reply := entry.(*Directory).Delete(client, entry)
		return handleReply(client, reply, 0)
	},
	"LIST": func (client *Client, arg string) error {
		return list(client, arg, nil)
	},
	"NLST": func (client *Client, arg string) error {
		return list(client, arg, func (client *Client, entry Entry, stream io.ReadWriter) Reply {
			for _, e := range entry.(*Directory).ListEntries() {
				_, err := stream.Write(append([]byte(e.Name()), '\r', '\n'))
				if err != nil {
					return Reply{451, client.Replies().TransmissionError}
				}
			}
			return Reply{226, client.Replies().ClosingData}
		})
	},
	"FEAT": func (client *Client, arg string) error {
		if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		}
		// A multiline reply is expected here; sendReply is incapable of that,
		// so we need to contruct one ourselves.
		_, err := client.control.Write([]byte(fmt.Sprint("211-", client.Replies().Features, "\r\n")))
		if err != nil {
			return err
		}

		_, err = client.control.Write([]byte(" UTF8\r\n MDTM\r\n SIZE\r\n REST STREAM\r\n TVFS\r\n"))
		if err != nil {
			return err
		}

		_, err = client.control.Write([]byte(" MLST "))
		if err != nil {
			return err
		}

		_, err = client.control.Write(formatFacts(client.facts))
		if err != nil {
			return err
		}

		_, err = client.control.Write([]byte("\r\n"))
		if err != nil {
			return err
		}

		if tlsConfig.Certificates != nil {
			_, err = client.control.Write([]byte(" AUTH TLS\r\n"))
			if err != nil {
				return err
			}
		}

		if conf.Languages != nil {
			size := 8
			for k := range conf.Languages {
				size += len(k) + 1
			}
			size--

			langs := make([]byte, 0, size)
			langs = append(langs, []byte(" LANG ")...)
			for k := range conf.Languages {
				langs = append(langs, []byte(k)...)
				if k == client.lang {
					langs = append(langs, '*')
				}
				langs = append(langs, ';')
			}
			// The last language should not end with a semicolon, so replace it with a carriage return
			langs[len(langs)-1] = '\r'
			langs = append(langs, '\n')
			_, err = client.control.Write(langs)
			if err != nil {
				return err
			}
		}
		_, err = client.control.Write([]byte(fmt.Sprint("211 ", client.Replies().End, "\r\n")))
		return err
	},
	"OPTS": func (client *Client, arg string) error {
		if arg == "" {
			return sendReply(client, 501, client.Replies().NoArgument)
		} else if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		}
		split := strings.SplitN(arg, " ", 2)
		switch strings.ToUpper(split[0]) {
		case "MLST":
			if len(split) != 1 {
				var reply []byte
				client.facts, reply = parseFacts(split[1])
				return sendReply(client, 200, string(append([]byte("OPTS MLST "), reply...)))
			} else {
				client.facts = 0
				return sendReply(client, 200, "OPTS MLST")
			}

		default:
			return sendReply(client, 501, client.Replies().UnknownOption)
		}
	},
	"LANG": func (client *Client, arg string) error {
		if conf.Languages == nil {
			return sendReply(client, 502, client.Replies().NotSupported)
		} else if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		} else if arg == "" {
			client.lang = conf.DefaultLanguage
			return sendReply(client, 200, conf.DefaultLanguage)
		}

		lang, ok := conf.Languages[arg]
		if !ok {
			return sendReply(client, 504, client.Replies().NoLanguage)
		}
		client.lang = arg
		if langCallback != nil {
			langCallback(client, arg)
		}
		return sendReply(client, 200, lang.Complete)
	},
	"MDTM": func (client *Client, arg string) error {
		if !client.loggedIn {
			return sendReply(client, 530, client.Replies().NotLoggedIn)
		} else if arg == "" {
			return sendReply(client, 501, client.Replies().NoArgument)
		}

		entry := client.Locate(arg)
		var date time.Time
		if entry == nil {
			return sendReply(client, 550, client.Replies().NotFound)
		} else if entry.ID() != 0 && entry.Parent().List == nil {
			return sendReply(client, 530, client.Replies().NoPrivileges)
		} else if entry.Directory() {
			date = entry.(*Directory).Modified
		} else {
			date = entry.(*File).Modified
		}
		if date.IsZero() {
			return sendReply(client, 550, client.Replies().NoDate)
		}
		return sendReply(client, 213, date.UTC().Format(FtpDateMilli))
	},
	"SIZE": func (client *Client, arg string) error {
		if !client.loggedIn {
			return sendReply(client, 530, client.Replies().NotLoggedIn)
		} else if arg == "" {
			return sendReply(client, 501, client.Replies().NoArgument)
		}

		entry := client.Locate(arg)
		if entry == nil {
			return sendReply(client, 550, client.Replies().NotFound)
		} else if entry.Directory() {
			return sendReply(client, 550, client.Replies().IsADirectory)
		} else if entry.Parent().List == nil {
			return sendReply(client, 530, client.Replies().NoPrivileges)
		} else if entry.(*File).Size < 0 {
			return sendReply(client, 550, client.Replies().NoSize)
		}
		return sendReply(client, 213, strconv.FormatInt(entry.(*File).Size, 10))
	},
	"MLST": func (client *Client, arg string) error {
		if !client.loggedIn {
			return sendReply(client, 530, client.Replies().NotLoggedIn)
		} else if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		}

		entry := client.Locate(arg)
		if entry == nil {
			return sendReply(client, 550, client.Replies().NotFound)
		}

		if entry.ID() != 0 && entry.Parent().List == nil {
			return sendReply(client, 530, client.Replies().NoPrivileges)
		}
		_, err := client.control.Write([]byte(fmt.Sprint("250-", client.Replies().List, "\r\n ")))
		if err != nil {
			return err
		}
		_, err = client.control.Write(formatMachine(client, entry))
		if err != nil {
			return err
		}
		_, err = client.control.Write([]byte(fmt.Sprint("250 ", client.Replies().End, "\r\n")))
		return err
	},
	"MLSD": func (client *Client, arg string) error {
		return list(client, arg, func (client *Client, entry Entry, stream io.ReadWriter) Reply {
			for _, e := range entry.(*Directory).ListEntries() {
				_, err := stream.Write([]byte(formatMachine(client, e)))
				if err != nil {
					return Reply{451, client.Replies().TransmissionError}
				}
			}
			return Reply{226, client.Replies().ClosingData}
		})
	},
	"AUTH": func (client *Client, arg string) error {
		if tlsConfig.Certificates == nil {
			return sendReply(client, 502, client.Replies().NotSupported)
		} else if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		}
		if len(arg) == 0 {
			return sendReply(client, 501, client.Replies().NoArgument)
		} else if arg != "TLS" {
			return sendReply(client, 504, client.Replies().NoEncryption)
		}

		err := sendReply(client, 234, client.Replies().Encrypting)
		if err != nil {
			return err
		}

		conn := tls.Server(client.control, &tlsConfig)

		// RFC-4217 requires handshake to begin immediately after an acknowledge,
		// so we have to begin the handshake manually.
		err = conn.Handshake()
		if err != nil {
			return sendReply(client, 421, client.Replies().EncryptionFailed)
		}

		client.control = conn
		client.tls = true
		return nil
	},
	"PBSZ": func (client *Client, arg string) error {
		// PBSZ doesn't really do anything on TLS, so being sloppy is okay here.
		if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		} else if !client.tls {
			return sendReply(client, 503, client.Replies().NotEncrypted)
		}

		return sendReply(client, 200, client.Replies().Complete)
	},
	"PROT": func (client *Client, arg string) error {
		if client.dataCallback != nil {
			return sendReply(client, 500, client.Replies().NotPossible)
		} else if arg == "" {
			return sendReply(client, 501, client.Replies().NoArgument)
		}
		if !client.tls {
			return sendReply(client, 503, client.Replies().NotEncrypted)
		}
		switch arg[0] {
		case 'C':
			client.tlsData = false
		case 'P':
			client.tlsData = true
		case 'E':
		case 'S':
			return sendReply(client, 534, client.Replies().NoProtect)
		default:
			return sendReply(client, 504, client.Replies().NoProtect)
		}
		return sendReply(client, 200, client.Replies().Complete)
	},
	"SITE": processSite,
}

var sequences []commandSeq = []commandSeq{
	commandSeq{
		"REST",
		func (client *Client, arg string, seq []string) (bool, error) {
			if !client.loggedIn {
				return false, sendReply(client, 530, client.Replies().NotLoggedIn)
			} else if client.dataCallback != nil {
				return false, sendReply(client, 500, client.Replies().NotPossible)
			} else if arg == "" {
				return false, sendReply(client, 501, client.Replies().NoArgument)
			}

			_, err := strconv.ParseUint(arg, 10, 64)
			if err != nil {
				return false, sendReply(client, 501, client.Replies().NotANumber)
			}
			return true, sendReply(client, 350, client.Replies().Restart)
		},
		[]commandSeq{
			// STOR and APPE is not supported for REST as there is no simple
			// way of implementing it.
			commandSeq{
				"APPE",
				func (client *Client, arg string, seq []string) (bool, error) {
					return false, sendReply(client, 502, client.Replies().NotSupported)
				},
				nil,
			},
			commandSeq{
				"RETR",
				func (client *Client, arg string, seq []string) (bool, error) {
					size, _ := strconv.ParseInt(seq[0], 10, 64)
					return false, retrieve(client, arg, size)
				},
				nil,
			},
			commandSeq{
				"STOR",
				func (client *Client, arg string, seq []string) (bool, error) {
					return false, sendReply(client, 502, client.Replies().NotSupported)
				},
				nil,
			},
		},
	},
	commandSeq{
		"RNFR",
		func (client *Client, args string, seq []string) (bool, error) {
			if !client.loggedIn {
				return false, sendReply(client, 530, client.Replies().NotLoggedIn)
			} else if client.dataCallback != nil {
				return false, sendReply(client, 500, client.Replies().NotPossible)
			} else if args == "" {
				return false, sendReply(client, 501, client.Replies().NoArgument)
			}

			entry := client.Locate(args)
			if entry == nil {
				return false, sendReply(client, 550, client.Replies().NotFound)
			} else if entry.Directory() {
				if entry.(*Directory).Rename == nil {
					return false, sendReply(client, 530, client.Replies().NoPrivileges)
				}
			} else {
				if entry.(*File).Rename == nil {
					return false, sendReply(client, 530, client.Replies().NoPrivileges)
				}
			}
			return true, sendReply(client, 350, client.Replies().Proceed)
		},
		[]commandSeq{
			commandSeq{
				"RNTO",
				func (client *Client, args string, seq []string) (bool, error) {
					if args == "" {
						return false, sendReply(client, 501, client.Replies().NoArgument)
					}

					if strings.ContainsRune(args, '/') {
						entry := client.LocateParent(args)
						if entry == nil {
							return false, sendReply(client, 553, client.Replies().NotFound)
						} else if !entry.Directory() {
							return false, sendReply(client, 553, client.Replies().NotADirectory)
						}
					}

					if client.Locate(args) != nil {
						return false, sendReply(client, 553, client.Replies().Exists)
					}

					args = client.Absolute(args)
					entry := client.Locate(seq[0])
					reply := Reply{}
					if entry.Directory() {
						reply = entry.(*Directory).Rename(client, entry, args)
					} else {
						reply = entry.(*File).Rename(client, entry, args)
					}
					return true, handleReply(client, reply, 0)
				},
				nil,
			},
		},
	},
	commandSeq{
		"USER",
		func (client *Client, buf string, seq []string) (bool, error) {
			if conf.ForceTLS && !client.tls {
				return false, sendReply(client, 530, client.Replies().TLSRequired)
			}

			return true, sendReply(client, 331, client.Replies().NeedPassword)
		},

		[]commandSeq{
			commandSeq{
				"PASS",
				func (client *Client, buf string, seq []string) (bool, error) {
					if !loginCallback(client, seq[0], buf) {
						return false, sendReply(client, 530, client.Replies().LoginFailed)
					}

					client.loggedIn = true
					return true, sendReply(client, 230, client.Replies().LoggedIn)
				},
				nil,
			},
		},
	},
}

func findSequence(seq []commandSeq, cmd string) *commandSeq {
	var c int
	var l, h int = 0, len(seq)-1
	var cmp int

	for l <= h {
		c = (l + h) / 2
		cmp = strings.Compare(cmd, seq[c].cmd)

		if cmp < 0 {
			h = c - 1
		} else if cmp > 0 {
			l = c + 1
		} else {
			return &seq[c]
		}
	}

	return nil
}

func retrieve(client *Client, arg string, skip int64) error {
	if !client.loggedIn {
		return sendReply(client, 530, client.Replies().NotLoggedIn)
	} else if client.dataCallback != nil {
		return sendReply(client, 500, client.Replies().NotPossible)
	} else if arg == "" {
		return sendReply(client, 501, client.Replies().NoArgument)
	}
	entry := client.Locate(arg)
	if entry == nil {
		return sendReply(client, 550, client.Replies().NotFound)
	}
	file, ok := entry.(*File)
	if !ok {
		return sendReply(client, 550, client.Replies().IsADirectory)
	} else if file.Read == nil {
		return sendReply(client, 530, client.Replies().NoPrivileges)
	}
	reply := file.Read(client, file)
	return handleReply(client, reply, skip)
}

func store(client *Client, path string, appe bool) error {
	var reply Reply

	entry := client.Locate(path)
	if entry == nil {
		entry, path = client.splitPath(path)
		if entry == nil {
			return sendReply(client, 553, client.Replies().NotFound)
		} else if !entry.Directory() {
			return sendReply(client, 553, client.Replies().NotADirectory)
		}
		dir := entry.(*Directory)
		if dir.Create == nil {
			return sendReply(client, 530, client.Replies().NoPrivileges)
		}
		reply = dir.Create(client, dir, path)
	} else {
		if entry.Directory() {
			return sendReply(client, 553, client.Replies().IsADirectory)
		} else if entry.(*File).Write == nil {
			return sendReply(client, 530, client.Replies().NoPrivileges)
		}
		file := entry.(*File)
		if appe {
			reply = file.Append(client, entry)
		} else {
			reply = file.Write(client, entry)
		}
	}
	return handleReply(client, reply, client.allo)
}

func list(client *Client, arg string, callback func (*Client, Entry, io.ReadWriter) Reply) error {
	var dir *Directory
	if !client.loggedIn {
		return sendReply(client, 530, client.Replies().NotLoggedIn)
	} else if client.dataCallback != nil {
		return sendReply(client, 500, client.Replies().NotPossible)
	}

	if len(arg) == 0 {
		dir = client.current
	} else {
		entry := client.Locate(arg)
		if entry == nil {
			return sendReply(client, 550, client.Replies().NotFound)
		} else if !entry.Directory() {
			return sendReply(client, 550, client.Replies().NotADirectory)
		}
		dir = entry.(*Directory)
	}
	if dir.List == nil {
		return sendReply(client, 530, client.Replies().NoPrivileges)
	}

	reply := dir.List(client, dir)
	if (reply.Code == 125 || reply.Code == 150) && client.dataCallback != nil {
		if callback != nil {
			client.dataEntry = dir
			client.dataCallback = callback
		}
		return dispatchData(client, 0)
	}
	return handleReply(client, reply, 0)
}

func formatMachine(client *Client, entry Entry) []byte {
	var buf []byte
	var size int

	permSize := func (ok bool) int {
		if ok {
			return 1
		}
		return 0
	}

	iLen := func (i int) (out int) {
		for i != 0 {
			out++
			i /= 10
		}
		return
	}

	appendFact := func (buf []byte, fact int, crit bool, name []byte, value string) []byte {
		if fact != 0 && crit {
			buf = append(buf, name...)
			buf = append(buf, '=')
			buf = append(buf, []byte(value)...)
			buf = append(buf, ';')
		}
		return buf
	}

	appendPerm := func (buf []byte, crit bool, b byte) []byte {
		if crit {
			buf = append(buf, b)
		}
		return buf
	}

	if client.facts & factUnique != 0 {
		size += 8 + iLen(entry.ID())
	}

	size += 3 + len(entry.Name())
	if entry.Directory() {
		dir := entry.(*Directory)
		if client.facts & factModify != 0 && !dir.Modified.IsZero() {
			size += 26
		}
		if client.facts & factCreate != 0 && !dir.Created.IsZero() {
			size += 26
		}
		if client.facts & factType != 0 {
			size += 9
		}
		if client.facts & factPerm != 0 {
			size += 6 + permSize(dir.Create != nil) + permSize(dir.Delete != nil) + permSize(dir.Access != nil) + permSize(dir.Rename != nil) + permSize(dir.List != nil) + permSize(dir.MakeDir != nil)
		}

		buf = make([]byte, 0, size)
		buf = appendFact(buf, client.facts & factModify, !dir.Modified.IsZero(), factModifyStr, dir.Modified.UTC().Format(FtpDateMilli))
		buf = appendFact(buf, client.facts & factCreate, !dir.Created.IsZero(), factCreateStr, dir.Created.UTC().Format(FtpDateMilli))
		buf = appendFact(buf, client.facts & factType, true, factTypeStr, "dir")
		buf = appendFact(buf, client.facts & factUnique, true, factUniqueStr, strconv.FormatInt(int64(dir.ID()), 10))
		if client.facts & factPerm != 0 {
			buf = append(buf, factPermStr...)
			buf = append(buf, '=')
			buf = appendPerm(buf, dir.Create != nil, 'c')
			buf = appendPerm(buf, dir.Delete != nil, 'd')
			buf = appendPerm(buf, dir.Access != nil, 'e')
			buf = appendPerm(buf, dir.Rename != nil, 'f')
			buf = appendPerm(buf, dir.List != nil, 'l')
			buf = appendPerm(buf, dir.MakeDir != nil, 'm')
			buf = append(buf, ';')
		}
	} else {
		file := entry.(*File)
		if client.facts & factSize != 0 && file.Size >= 0 {
			size += iLen(int(file.Size))
		}
		if client.facts & factModify != 0 && !file.Modified.IsZero() {
			size += 26
		}
		if client.facts & factCreate != 0 && !file.Created.IsZero() {
			size += 26
		}
		if client.facts & factType != 0 {
			size += 10
		}
		if client.facts & factPerm != 0 {
			size += 6 + permSize(file.Append != nil) + permSize(file.Delete != nil) + permSize(file.Rename != nil) + permSize(file.Read != nil) + permSize(file.Write != nil)
		}
		if client.facts & factLang != 0 && file.Language != "" {
			size += 6 + len(file.Language)
		}
		if client.facts & factMediaType != 0 && file.MediaType != "" {
			size += 10 + len(file.MediaType)
		}
		if client.facts & factLang != 0 && file.Charset != "" {
			size += 7 + len(file.Charset)
		}

		buf = make([]byte, 0, size)
		buf = appendFact(buf, client.facts & factSize, file.Size >= 0, factSizeStr, strconv.FormatInt(file.Size, 10))
		buf = appendFact(buf, client.facts & factModify, !file.Modified.IsZero(), factModifyStr, file.Modified.UTC().Format(FtpDateMilli))
		buf = appendFact(buf, client.facts & factCreate, !file.Created.IsZero(), factCreateStr, file.Created.UTC().Format(FtpDateMilli))
		buf = appendFact(buf, client.facts & factType, true, factTypeStr, "file")
		buf = appendFact(buf, client.facts & factUnique, true, factUniqueStr, strconv.FormatInt(int64(file.ID()), 10))
		if client.facts & factPerm != 0 {
			buf = append(buf, factPermStr...)
			buf = append(buf, '=')
			buf = appendPerm(buf, file.Append != nil, 'a')
			buf = appendPerm(buf, file.Delete != nil, 'd')
			buf = appendPerm(buf, file.Rename != nil, 'f')
			buf = appendPerm(buf, file.Read != nil, 'r')
			buf = appendPerm(buf, file.Write != nil, 'w')
			buf = append(buf, ';')
		}
		buf = appendFact(buf, client.facts & factLang, file.Language != "", factLangStr, file.Language)
		buf = appendFact(buf, client.facts & factMediaType, file.MediaType != "", factMediaTypeStr, file.MediaType)
		buf = appendFact(buf, client.facts & factCharset, file.Charset != "", factCharsetStr, file.Charset)
	}

	buf = append(buf, ' ')
	buf = append(buf, []byte(entry.Name())...)
	buf = append(buf, '\r', '\n')
	return buf
}
