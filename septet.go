
/*
Package septet implements an API for development of the FTP protocol. The
API provides an abstract filesystem which can be programatically created and
altered dynamically, without the restrictions of a virtual filesystem.

Note that septet, while simple, is not an easy-to-use package, and requires a
substantial amount of knowledge of the FTP protocol to use. The specification
for the FTP protocol is the RFC-959, which can be found at
[ftp://ietf.org/rfc/rfc959.txt].

Events

Virtual filesystems are manipulated through events. Each client has it's own
unique filesystem, which, initially, only contains the root directory. The
filesystem should be populated after a successful login, in which the client
is then able to perform operations on the filesystem. Events will then fire
for each operation performed; see Entry for more information.

Some events are required to open the data connection in order to send or
receieve information between the client and the server. Read and Write in
File is an example of that; if the server is able to transfer information
to or from the client, OpenData() should be called and the Reply returned from
that function should be returned from the event. The callback function, once
called, can then send or retrieve data from the client.

Login and Authentication

When a new client clients tries to authenticate, the login callback is
called for that client. Because the client cannot access the client tree
before this, it's often useful to use an "anonymous" login for users that
doesn't have an account for that corresponding server. This account's
username is, appropriately, "anonymous", and the password is ignored.

Valid usernames and passwords need to be managed manually. A practical way
would be to use a database in the background, storing username and
password information along with an ID. The client can then store the
ID for the user the client is logged in at, or 0 if the client is logged in
anonymously (if that is implemented on the server).

A possible implementation could look like this:

	func Login(client *septet.Client, user string, pass *string) bool {
		var id int
		db, err := sql.Open("sqlite3", "database.db3")
		if err != nil {
			return false
		}

		defer db.Close()

		if pass == nil {
			row := db.QueryRow("SELECT COUNT(id) FROM login WHERE user = ?", user)
			err = row.Scan(&id)
			if err != nil {
				return false
			}
			return id > 0
		} else {
			row := db.QueryRow("SELECT id FROM login WHERE user = $1 AND pass = $2", user, *pass)
			err = row.Scan(&id)
			if err != nil {
				return false
			}
			// TODO: initialize client tree.
			client.Data = id
			return true
		}
	}

Note that the example above does not take security in consideration, and
shouldn't be used for production.

Commands and Replies

FTP works in a call-and-response order; the client sends a command and the
server responds with a reply. septet enforces this behaviour by only letting
a single reply to be sent when returning from an event.

It is important to note that replies in FTP can have side effects. For example,
150 will cause the server to attempt to open the data connection. This reply
should be accompanied with OpenData to prepare the server for opening the data
port, as returning 150 directly is undefined. Additionally, one reply to keep
in mind is 421, which, when sent, will automatically close the connection to
the client. This should be favoured over Close(), as it also allows giving the
client an closing message along with the close.

Sessions

Unlike most other protocols, FTP is a hybrid between stateless sessions such as
HTTP and stateful sessions like Telnet. Each session has states, in the sense
that the connection is not closed when a file transfer or directory listing is
done, but due to the nature of FTP, and the fact that it's designed to mirror a
physical filesystem, some clients can eagerly close and open sessions as well
as locally cache the filesystem in order to optimize connection to the server.

This consequently leads to heavy limitations on what is possible in FTP,
despite being allowed according to standards. To avoid this, it is generally
recommended to avoid session-oriented features on the FTP server altogether,
and in case a session-oriented feature is vital, rely on a timer rather than
the actual session. This is also the reason why septet doesn't have an event
for clients disconnecting and connecting.

Data Connection Limitations

When data transfer between the client and the server is done, the transfer is
done on a different connection. This connection will only last during the
transfer, and is closed when the transfer is done. However, due to the fact of
how FTP connections work, the state of the transfer must be communicated over
the control connection. While this is okay is most circumstances, there is one
combination of data connection configuration that falls against the server's
odds, and is unfortunatly the most common one.

Data can be transferred in various formats. The three officially supported are
File, Record and Page structure. septet only supports File and Record, as they
are the ones that are mandatory to be implemented, as well as the fact that
Page is hardly ever used. The main difference is that Record is able to mark
EOR and EOF in the data stream, making it possible for the client to tell when
the file is fully sent. This is done by treating the value 0xff as an escape
character that is followed by either 0x01 (EOR), 0x02 (EOF) or 0xff (single
0xff, for sending binary containing 0xff).

The problem with File, however, is that it doesn't have any method of marking 
EOF, and therefore is unable to tell the server when a transfer is done. This
requires the client to send the amount of data to be send before the transfer
has started using the ALLO command. If the client fails to do so, the transfer
is rejected.

The above, however, is only true for storing. Retrieving does not have this
limitation, as the server is able to inform the client of EOF by sending a 226
reply over the control connection, telling the client that the server is
closing the data connection. This can be interpreted as EOF, as the server will
not send any more information to the client.
*/
package septet

import (
	"errors"
)

// Common runtime errors for file operations.
var (
	ErrExist = errors.New("entry already exists")
	ErrNotExist = errors.New("entry does not exists")
	ErrIsDirectory = errors.New("entry is a directory")
	ErrNotDirectory = errors.New("entry is a not directory")
	ErrTimedOut = errors.New("timed out")
	ErrClosed = errors.New("connection closed")
)

const (
	FtpDate      = "20060102150405"
	FtpDateMilli = "20060102150405.000"
)
