
# Septet - An FTP Server package for Go

Septet is a programmable server for FTP.

## Install

To install, simply run:

```
go get gitlab.com/Hanicef/septet
```
