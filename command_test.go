
package septet

import (
	"testing"
	"time"
	"strings"
)

func TestCommands(test *testing.T) {
	for k := range commands {
		if strings.ToUpper(k) != k {
			test.Errorf("%s is not uppercased", k)
		}
	}

	// Sequences must be ordered, as binary search is used to locate sequences.
	seqList := [][]commandSeq{sequences}
	for len(seqList) > 0 {
		for l, s := range seqList[0] {
			for i := 0; i < l; i++ {
				if strings.ToUpper(s.cmd) != s.cmd {
					test.Errorf("%s is not uppercased", s.cmd)
				}
				if strings.Compare(s.cmd, seqList[0][i].cmd) <= 0 {
					test.Errorf("Sequence list unordered! %s should be before %s", s.cmd, seqList[0][i].cmd)
					break
				}
			}
			if s.next != nil {
				seqList = append(seqList, s.next)
			}
		}
		seqList = seqList[1:]
	}
}

func TestFindCommand(test *testing.T) {
	for _, s := range sequences {
		seq := findSequence(sequences, s.cmd)
		if seq == nil {
			test.Error(s.cmd, "!= nil failed")
			continue
		}
		if seq.cmd != s.cmd {
			test.Error(s.cmd, "==", seq.cmd, "failed")
		}
	}
}

func TestFormatMachine(t *testing.T) {
	check := func (client *Client, entry Entry, expect string) {
		got := string(formatMachine(client, entry))
		if got != expect {
			t.Errorf("formatMachine failed! Expected \n%s but got \n%s", expect, got)
		}
	}

	modified, _ := time.Parse(time.RFC3339, "2017-03-21T08:34:50Z")
	created, _ := time.Parse(time.RFC3339, "2017-01-08T09:21:24Z")
	client := Client{
		facts: factSize | factModify | factCreate | factType,
	}
	dir := Directory{
		name: "foo",
		Modified: modified,
		Created: created,
	}
	check(&client, &dir, "modify=20170321083450.000;create=20170108092124.000;type=dir; foo\r\n")

	client.facts |= factPerm | factCharset
	file := File{
		name: "bar",
		Size: 34,
		Modified: modified,
		Created: created,
		Charset: "utf8",
		Read: func (client *Client, entry Entry) Reply {
			return Reply{0, ""}
		},
	}
	check(&client, &file, "size=34;modify=20170321083450.000;create=20170108092124.000;type=file;perm=r;charset=utf8; bar\r\n")
}
