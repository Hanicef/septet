
package septet

import (
	"errors"
	"time"
	"strings"
)

// Entry is an interface for an entry in the tree. It has two implementations:
// File and Directory. File is a regular file that can be read from or written
// to; Directory is a directory that can contain other entries.
//
// File and Directory has events that can file based on what operation the
// client performs. If a client tries to perform an event that is set to nil,
// the server will respond with insufficient privileges. This is important for
// privilege control; clients that shouldn't have access to an operation should
// have the corresponding event set to nil. This will also be reflected on MLST
// and MLSD replies, in which the privileges is set based on which events are
// nil and which ones are not.
//
// For more details on specific events, see File and Directory.
type Entry interface {
	// ID returns the entry's unique ID.
	ID() int

	// Client returns the client that this entry belongs to, or nil if the
	// entry is not registered yet.
	Client() *Client

	// Name returns the name of the entry.
	Name() string

	// SetName sets the name of the entry.
	SetName(name string) error

	// Directory returns true if this entry is a directory; false otherwise.
	Directory() bool

	// Parent returns the parent directory of the entry, or nil if the entry 
	// is the root directory.
	Parent() *Directory

	// Path returns the absolute path to the entry.
	Path() string

	setClient(client *Client)
	setParent(*Directory)
}

// File contains information on a specific file.
type File struct {
	id int
	client *Client
	parent *Directory

	name string

	// Size is the size in bytes of the file. This can be useful for clients to
	// make sure ahead of time there is enough disk space for the file to be
	// stored on the client's disk. Setting this to a negative value indicates
	// the file size is unknown or not available.
	Size int64

	// Modified is a date of which the file was last modified. This can be used
	// by clients to make sure a store can be restarted after a store; if it
	// doesn't match the date when the file was stored by the client, the file
	// store is restarted from the beginning. Setting this to zero time
	// (January 1, year 1, 00:00:00 UTC) indicates the last modified time is
	// unknown or not available.
	Modified time.Time

	// Created is a date of which the file was originally created. This is only
	// used as information to the client. Setting this to zero time (January 1,
	// year 1, 00:00:00 UTC) indicates the created time is unknown or not
	// available.
	Created time.Time

	// Language is the language of the content of the file. An empty string
	// indicates that the language is unknown or not available.
	Language string

	// MediaType is the MIME type of the file. An empty string indicates that
	// the MIME type is unknown or not available.
	MediaType string

	// Charset is the IANA character set name of the file. This is used to tell
	// what the file is encoded in, so the client can convert it to it's local
	// encoding scheme. Note that it's not really a good idea to specify a
	// character encoding on a binary file, as the client may alter in after
	// retrieval. Setting this to an empty string will omit this value.
	Charset string

	// Data is an interface that can be used to store any additional data in a
	// file. It will not be used or modified internally.
	Data interface{}

	// Append is called whenever new data is appended to this file.
	//
	// Allowed replies:
	//  * 125, 150 - File OK, opening data connection.
	//     * Side effect: if OpenData is called, the callback will be called.
	//  * 450 - File cannot be modified at the moment.
	//  * 550 - File is not allowed to be altered.
	//  * 452 - No space available to write data.
	Append func (client *Client, entry Entry) Reply

	// Delete is called whenever a delete is attempted on this file.
	//
	// Allowed replies:
	//  * 250 - Delete successful.
	//  * 450 - Cannot delete at the moment.
	//  * 550 - File is not allowed to be deleted.
	Delete func (client *Client, entry Entry) Reply

	// Rename is called whenever a rename is attempted on this file.
	//
	// The newName parameter contains the absolute path of where the file is
	// to be renamed to. This doesn't neccesarily have to point to the current
	// directory, nor does the file have to exist; use LocateParent to find the
	// directory that the client is requesting the file to be moved to.
	//
	// Allowed replies:
	//  * 250 - Rename successful.
	//  * 553 - Cannot rename to this directory.
	Rename func (client *Client, entry Entry, newName string) Reply

	// Read is called whenever the file is retrieved by the client.
	//
	// Allowed replies:
	//  * 125, 150 - File OK, opening data connection.
	//     * Side effect: if OpenData is called, the callback will be called.
	//  * 450 - File cannot be retrieved at the moment.
	//  * 550 - File is not allowed to be retrieved.
	Read func (client *Client, entry Entry) Reply

	// Write is called whenever new data is stored in this file.
	//
	// Allowed replies:
	//  * 125, 150 - File OK, opening data connection.
	//     * Side effect: if OpenData is called, the callback will be called.
	//  * 450 - File cannot be modified at the moment.
	//  * 452 - No space available to write data.
	Write func (client *Client, entry Entry) Reply
}

// Directory contains information on a specific directory.
type Directory struct {
	id int
	client *Client
	parent *Directory
	entries []Entry

	name string

	// Modified is a date of which the file was last modified. This is only
	// used as information to the client and doesn't have to be correct.
	// Setting this to zero time (January 1, year 1, 00:00:00 UTC) will disable
	// sending the modified time.
	Modified time.Time

	// Created is a date of which the file was originally created. This is only
	// used as information to the client and doesn't have to be correct.
	// Setting this to zero time (January 1, year 1, 00:00:00 UTC) will disable
	// sending the created time.
	Created time.Time

	// Data is an interface that can be used to store any additional data in a
	// directory. It will not be used or modified internally.
	Data interface{}

	// Create is called whenever a file is created in this directory.
	//
	// The name parameter is the name of the file to store. This is guranteed
	// to be unique for the directory.
	//
	// Allowed replies:
	//  * 125, 150 - File OK, opening data connection.
	//     * Side effect: if OpenData is called, the callback will be called.
	//  * 450 - File cannot be created at the moment.
	//  * 452 - No space available to write data.
	//  * 553 - File name is not allowed in this directory.
	Create func (client *Client, entry Entry, name string) Reply

	// Delete is called whenever a delete is attempted on this directory.
	//
	// Allowed replies:
	//  * 250 - Delete successful.
	//  * 550 - File is not allowed to be deleted.
	Delete func (client *Client, entry Entry) Reply

	// Access is called whenever a client is entering the directory.
	//
	// Allowed replies:
	//  * 250, 200 - File access granted
	//     * Side effect: client's current directory changes.
	//  * 550 - File not allowed to be accessed.
	Access func (client *Client, entry Entry) Reply

	// Rename is called whenever a rename is attempted on this directory.
	//
	// The newName parameter contains the absolute path of where the file is
	// to be renamed to. This doesn't neccesarily have to point to the current
	// directory, nor does the file have to exist; use LocateParent to find the
	// directory that the client is requesting the file to be moved to. The
	// name is guranteed to be unique for that directory.
	//
	// Allowed replies:
	//  * 250 - Rename successful.
	//  * 553 - Cannot rename to this directory.
	Rename func (client *Client, entry Entry, newName string) Reply

	// List is called whenever a list is attempted on this directory.
	//
	// Note that List is treated differently from other events in that way that
	// the callback specified in OpenData may not be entered if the client
	// requested machine listing. This is a special case where the data sent
	// on the data connection is required to strictly follow a specific format
	// to make parsing of the data connection possible on the client side. In
	// that case, an internal callback will be called instead to automate this
	// process.
	//
	// Allowed replies:
	//  * 125, 150 - Listing okay.
	//     * Side effect: if OpenData is called, the callback will be called.
	//  * 450 - Cannot list files at the moment.
	List func (client *Client, entry Entry) Reply

	// MakeDir is called whenever a directory is created in this file.
	//
	// The name parameter is the name of the directory to be created. This is
	// guranteed to be unique for this directory.
	//
	// Allowed replies:
	//  * 257 - Directory creation successful.
	//  * 550 - Directory is not allowed to be created.
	MakeDir func (client *Client, entry Entry, name string) Reply
}

func (f *File) ID() int {
	return f.id
}

func (f *File) Client() *Client {
	return f.client
}

func (f *File) Name() string {
	return f.name
}

func (f *File) SetName(name string) error {
	if len(name) == 0 {
		return errors.New("name must not be empty")
	} else if strings.ContainsRune(name, '/') {
		return errors.New("name must not contain forward slash")
	} else if f.Parent() != nil && f.Parent().Get(name) != nil {
		return ErrExist
	}
	f.name = name
	return nil
}

func (f *File) Directory() bool {
	return false
}

func (f *File) Parent() *Directory {
	return f.parent
}

func (f *File) Path() string {
	var path []byte
	var size int = len(f.name) + 1
	var dir *Directory = f.parent

	if dir == nil {
		return f.name
	}

	for dir.id != 0 {
		size += len(dir.name) + 1
		dir = dir.parent
	}

	path = make([]byte, size)
	path[0] = '/'
	size = copy(path[1:], []byte(f.name)) + 1

	dir = f.parent
	for dir.id != 0 {
		copy(path[len(dir.name) + 1:len(dir.name) + size + 1], path[:size])
		path[0] = '/'
		size += copy(path[1:], []byte(dir.name)) + 1

		dir = dir.parent
	}

	return string(path)
}

func (f *File) setClient(client *Client) {
	f.client = client
}

func (f *File) setParent(dir *Directory) {
	f.parent = dir
}

func (d *Directory) ID() int {
	return d.id
}

func (d *Directory) Client() *Client {
	return d.client
}

func (d *Directory) Name() string {
	return d.name
}

func (d *Directory) SetName(name string) error {
	if len(name) == 0 {
		return errors.New("name must not be empty")
	} else if strings.ContainsRune(name, '/') {
		return errors.New("name must not contain forward slash")
	} else if d.Parent() != nil && d.Parent().Get(name) != nil {
		return ErrExist
	}
	d.name = name
	return nil
}

func (d *Directory) Directory() bool {
	return true
}

func (d *Directory) Parent() *Directory {
	return d.parent
}

func (d *Directory) Path() string {
	var path []byte
	var size int
	var dir *Directory = d

	if dir.id == 0 {
		return "/"
	}

	for dir.id != 0 {
		size += len(dir.name) + 1
		dir = dir.parent
	}

	path = make([]byte, size)
	size = 0

	dir = d
	for dir.id != 0 {
		copy(path[len(dir.name) + 1:len(dir.name) + size + 1], path[:size])
		path[0] = '/'
		size += copy(path[1:], []byte(dir.name)) + 1

		dir = dir.parent
	}

	return string(path)
}

func (d *Directory) setClient(client *Client) {
	d.client = client
}

func (d *Directory) setParent(dir *Directory) {
	d.parent = dir
}

// ListEntries returns a slice of entries within the directory.
func (d *Directory) ListEntries() []Entry {
	return d.entries
}

// GetByID searches for id in the directory and returns a match if any is
// found, otherwise nil.
func (d *Directory) GetByID(id int) Entry {
	for _, e := range d.entries {
		if e.ID() == id {
			return e
		}
	}

	return nil
}


// Get searches for name in the directory and returns a match if any is found,
// otherwise nil.
func (d *Directory) Get(name string) Entry {
	for _, e := range d.entries {
		if e.Name() == name {
			return e
		}
	}

	return nil
}

// Find searches for name in the entries within the directory and all
// subdirectories and returns all matches found, or nil if none was found.
//
// This function could be very slow, especially with large file trees. If
// applicable, it's recommended that Get is used instead.
func (d *Directory) Find(name string) []Entry {
	var subdirs []*Directory = []*Directory{d}
	var matches []Entry = nil
	for i := 0; i < len(subdirs); i++ {
		for _, e := range subdirs[i].entries {
			if e.Name() == name {
				matches = append(matches, e)
			} else if e.Directory() {
				subdirs = append(subdirs, e.(*Directory))
			}
		}
	}

	return matches
}

// Add adds entries to the directory. Adding an entry with a name that already
// exists will return an error.
func (d *Directory) Add(entries ...Entry) error {
	if d.Client() == nil {
		return errors.New("directory must be registered first")
	}
	for _, entry := range entries {
		if entry == nil {
			return errors.New("entry is nil")
		}

		if entry.Client() == nil {
			return errors.New("entries must be created through the client")
		}

		if entry.Client() != d.Client() {
			return errors.New("cannot put other clients' entries in this tree")
		}

		for _, e := range d.entries {
			if e.Name() == entry.Name() {
				return ErrExist
			}
		}
	}

	for _, entry := range entries {
		entry.setParent(d)
		d.entries = append(d.entries, entry)
	}
	return nil
}

// Remove removes entry from the directory. An error will be returned if the
// entry is not located in this directory.
func (d *Directory) Remove(entries... Entry) error {
	for _, entry := range entries {
		if entry == nil {
			return errors.New("entry is nil")
		}

		if dir, ok := entry.(*Directory); ok {
			if dir.ID() == 0 {
				return errors.New("cannot remove root directory")
			}

			cwd := d.Client().CurrentDirectory()
			for cwd.Parent() != nil {
				if cwd == dir {
					return errors.New("cannot remove a directory the client is located in")
				}
				cwd = dir.Parent()
			}
		}

		if entry.Parent() != d {
			return ErrNotExist
		}
	}

next:
	for _, entry := range entries {
		for i := range d.entries {
			if d.entries[i] == entry {
				d.entries[i].setParent(nil)
				for ; i < len(d.entries)-1; i++ {
					d.entries[i] = d.entries[i+1]
				}
				d.entries = d.entries[:i]
				continue next
			}
		}

		panic("parent does not match actual entry location")
	}
	return nil
}

// Move moves the specified entry from the current directory the entry is
// located in to this directory. If the entry has not yet been added to the
// tree, this function has the same effect as Add.
func (d *Directory) Move(entry Entry) error {
	if entry == nil {
		return errors.New("entry is nil")
	}
	return d.MoveName(entry, entry.Name())
}

// MoveName is similar to Move, but also sets the entry name to name.
func (d *Directory) MoveName(entry Entry, name string) error {
	var err error
	if entry == nil {
		return errors.New("entry is nil")
	}

	if entry.ID() == 0 {
		return errors.New("cannot move root directory")
	}

	if entry.Client() == nil {
		return errors.New("entries must be created through the client")
	}

	if entry.Client() != d.Client() {
		return errors.New("cannot put other clients' entries in this tree")
	}

	for _, e := range d.entries {
		if e.Name() == name {
			return ErrExist
		}
	}

	if len(name) == 0 {
		return errors.New("name must not be empty")
	} else if strings.ContainsRune(name, '/') {
		return errors.New("name must not contain forward slash")
	}

	if entry.Parent() != nil {
		err = entry.Parent().Remove(entry)
		if err != nil {
			return err
		}
	}

	entry.SetName(name)
	entry.setParent(d)
	d.entries = append(d.entries, entry)
	return nil
}

// Clear removes all entries within the directory.
func (d *Directory) Clear() error {
	var current = d.Client().current
	for current != nil {
		current = current.Parent()
		if current == d {
			return errors.New("cannot remove a directory the client is located in")
		}
	}
	for i := range d.entries {
		d.entries[i].setParent(nil)
		d.entries[i] = nil
	}
	d.entries = d.entries[:0]
	return nil
}

func (c *Client) NewFile(name string) (*File, error) {
	c.idCounter++

	f := new(File)
	f.id = c.idCounter
	f.client = c
	return f, f.SetName(name)
}

func (c *Client) NewDirectory(name string) (*Directory, error) {
	c.idCounter++

	d := new(Directory)
	d.id = c.idCounter
	d.client = c
	return d, d.SetName(name)
}

// RegisterEntry registers an entry. This returns a copy of the entry with a
// new ID and registers the entry to the client it belongs to.
//
// This function can also be used to create a copy of an existing entry. If the
// entry is a directory, all the subentries will also be registered along with
// the directory.
func (c *Client) RegisterEntry(e Entry) (Entry, error) {
	var err error
	if e.Name() == "" {
		return nil, errors.New("no name is set for entry")
	}
	if e.Directory() {
		dir := e.(*Directory)
		for i := range dir.ListEntries() {
			dir.entries[i], err = c.RegisterEntry(dir.entries[i])
			if err != nil {
				return nil, err
			}
		}
		out := new(Directory)
		*out = *dir
		c.idCounter++
		out.id = c.idCounter
		out.client = c
		return out, nil
	} else {
		out := new(File)
		*out = *e.(*File)
		c.idCounter++
		out.id = c.idCounter
		out.client = c
		return out, nil
	}
}

// RootDirectory returns the root directory of the client.
func (c *Client) RootDirectory() *Directory {
	return &c.root
}

// CurrentDirectory returns the current directory of the client.
func (c *Client) CurrentDirectory() *Directory {
	return c.current
}

// Locate attempts to locate a file in a client's tree at the specified path,
// and returns the entry located at path if one was found, otherwise nil.
func (c *Client) Locate(path string) Entry {
	var current Entry
	var loc []string

	if len(path) == 0 {
		return c.current
	}

	path = strings.TrimRight(path, "/")
	if len(path) == 0 {
		path = "/"
	}

	if path[0] == '/' {
		if len(path) == 1 {
			return &c.root
		}
		loc = strings.Split(path[1:], "/")
		current = &c.root
	} else {
		loc = strings.Split(path, "/")
		current = c.current
	}
	for i := range loc {
		next := current.(*Directory).Get(loc[i])
		if next == nil || (i < len(loc) - 1 && !next.Directory()) {
			return nil
		}
		current = next
	}
	return current
}

// LocateParent attempts to locate the directory in a client's tree of which an
// entry name is located. The entry doesn't have to exist; only the parent
// directories have to exist. The parent directory is returned if it was found;
// if not, nil is returned.
func (c *Client) LocateParent(path string) *Directory {
	path = strings.TrimRight(path, "/")
	if len(path) == 0 {
		return &c.root
	}
	par := strings.LastIndexByte(path, '/')
	if par == -1 {
		return c.current
	} else if par == 0 {
		return &c.root
	} else {
		entry := c.Locate(path[:par])
		if entry == nil || !entry.Directory() {
			return nil
		}
		return entry.(*Directory)
	}
}

