
package septet

import (
	"strings"
	"errors"
)

var siteCommands map[string]func (*Client, string) Reply

// SetSiteCommand sets a SITE command. SITE commands can be invoked by the client
// to allow performing non-standard operations on an FTP server, and has no
// restrictions on behaviour or return values.
//
// Passing nil to callback will unset a SITE command.
//
// An error is returned if command contains a space. Additionally, attempting
// to set a site command while the server is running will result in a panic.
func SetSiteCommand(command string, callback func (*Client, string) Reply) error {
	if listenSocket != nil {
		panic("attempted to set site command when server is running")
	}
	if strings.Contains(command, " ") {
		return errors.New("command may not contain spaces")
	}
	if siteCommands == nil {
		siteCommands = make(map[string]func (*Client, string) Reply, 1)
	}
	if callback == nil {
		delete(siteCommands, command)
	} else {
		siteCommands[strings.ToUpper(command)] = callback
	}
	return nil
}

func processSite(client *Client, arg string) error {
	var cmd string

	if arg == "" {
		return sendReply(client, 501, client.Replies().NoArgument)
	}

	i := strings.IndexByte(arg, ' ')
	if i == -1 {
		cmd = arg
		arg = ""
	} else {
		cmd = arg[:i]
		arg = arg[i+1:]
	}

	callback, ok := siteCommands[strings.ToUpper(cmd)]
	if !ok {
		return sendReply(client, 500, client.Replies().UnknownCommand)
	} else {
		reply := callback(client, arg)
		if (reply.Code == 125 || reply.Code == 150) && client.dataCallback != nil {
			return dispatchData(client, 0)
		}
		return sendReply(client, reply.Code, reply.Message)
	}
}
