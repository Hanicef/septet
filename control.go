
package septet

import (
	"errors"
	"time"
	"fmt"
	"os"
	"net"
)

var errLineTooLong = errors.New("line too long")
var backBuffer []byte

const lineLength = 128

func readLine(client *Client, buf []byte, timeout time.Duration) ([]byte, error) {
	if backBuffer != nil {
		buf = backBuffer
		backBuffer = nil
		return buf, nil
	}

	for i := 0; i < len(buf) - 1; i += 2 {
		err := client.control.SetReadDeadline(time.Now().Add(timeout))
		if err != nil {
			return nil, err
		}

		_, err = client.control.Read(buf[i:i+2])
		if err != nil {
			return nil, err
		}

		if buf[i] == '\r' && buf[i+1] == '\n' {
			return buf[:i], nil
		}

		for buf[i+1] == '\r' {
			// Handle cases where two carriage returns come in succession.
			i++
			err = client.control.SetReadDeadline(time.Now().Add(timeout))
			if err != nil {
				return nil, err
			}

			_, err := client.control.Read(buf[i+1:i+2])
			if err != nil {
				return nil, err
			}

			if buf[i+1] == '\n' {
				return buf[:i], nil
			}
		}
	}

	// Await end of line before returning.
	for buf[0] != '\r' || buf[1] != '\n' {
		err := client.control.SetReadDeadline(time.Now().Add(timeout))
		if err != nil {
			return nil, err
		}

		_, err = client.control.Read(buf[:])
		if err != nil {
			return nil, err
		}
		for buf[1] == '\r' {
			// Handle cases where two carriage returns come in succession.
			err = client.control.SetReadDeadline(time.Now().Add(timeout))
			if err != nil {
				return nil, err
			}

			buf[0] = buf[1]
			_, err = client.control.Read(buf[1:])
			if err != nil {
				return nil, err
			}
		}
	}

	return nil, errLineTooLong
}

func handleClient(client *Client) {
	var err error
	var buf []byte = make([]byte, lineLength)
	var data []byte
	var cmd string
	var arg string
	var last []string
	var i int
	var ok bool
	var activeSeq []commandSeq
	var seq *commandSeq
	var activeCmd command

	defer client.Close()
	err = sendReply(client, 220, client.Replies().Ready)
	if err != nil {
		return
	}

	for client.isOpen() {
		data, err = readLine(client, buf, conf.Timeout)
		if err != nil {
			if err == errLineTooLong {
				err = sendReply(client, 500, client.Replies().LineTooLong)
				if err != nil {
					return
				}
				continue
			} else if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
				sendReply(client, 421, client.Replies().TimedOut)
				return
			} else {
				sendReply(client, 421, client.Replies().ConnectionError)
				return
			}
		}

		arg = ""
		for i = 0; i < len(data); i++ {
			if data[i] == ' ' {
				if i + 1 < len(data) {
					arg = string(data[i+1:])
				}
				data = data[:i]
				break
			} else if data[i] >= 'a' && data[i] <= 'z' {
				// Keep all letters uppercase to help in case-insensitivity.
				data[i] -= 'a' - 'A'
			}
		}
		cmd = string(data)

		if activeSeq == nil {
			activeSeq = sequences
		}
		seq = findSequence(activeSeq, cmd)
		if seq == nil {
			last = last[:0]
			activeSeq = nil
			activeCmd, ok = commands[cmd]
			if !ok {
				err = sendReply(client, 500, client.Replies().UnknownCommand)
				if err != nil {
					fmt.Fprintf(os.Stderr, "sendReply: %s\n", err.Error())
					return
				}
			} else {
				err = activeCmd(client, arg)
				if err != nil {
					fmt.Fprintf(os.Stderr, "%s: %s\n", cmd, err.Error())
					return
				}
			}
		} else {
			ok, err = seq.function(client, arg, last)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%s: %s\n", cmd, err.Error())
				return
			}

			if seq.next == nil {
				activeSeq = nil
				last = last[:0]
			} else {
				activeSeq = seq.next
				last = append(last, arg)
			}
		}
	}
}
