
package septet

import (
	"strings"
)

const (
	factSize      = 1
	factModify    = 2
	factCreate    = 4
	factType      = 8
	factUnique    = 16
	factPerm      = 32
	factLang      = 64
	factMediaType = 128
	factCharset   = 256
)

var (
	factSizeStr = []byte("size")
	factModifyStr = []byte("modify")
	factCreateStr = []byte("create")
	factTypeStr = []byte("type")
	factUniqueStr = []byte("unique")
	factPermStr = []byte("perm")
	factLangStr = []byte("lang")
	factMediaTypeStr = []byte("media-type")
	factCharsetStr = []byte("charset")
)

var factList = [][]byte{
	factSizeStr,
	factModifyStr,
	factCreateStr,
	factTypeStr,
	factUniqueStr,
	factPermStr,
	factLangStr,
	factMediaTypeStr,
	factCharsetStr,
}

func formatFacts(facts int) []byte {
	size := 0
	for i := range factList {
		size += len(factList[i]) + 1
		if facts & (1<<uint(i)) != 0 {
			size++
		}
	}

	buf := make([]byte, 0, size)
	for i := range factList {
		buf = append(buf, factList[i]...)
		if facts & (1<<uint(i)) != 0 {
			buf = append(buf, '*')
		}
		buf = append(buf, ';')
	}

	return buf
}

func parseFacts(facts string) (int, []byte) {
	var out int
	var reply = make([]byte, 0, len(facts))

	factList := strings.Split(facts, ";")
	for _, f := range factList {
		last := out
		switch f {
		case string(factSizeStr):
			out |= factSize
		case string(factModifyStr):
			out |= factModify
		case string(factCreateStr):
			out |= factCreate
		case string(factTypeStr):
			out |= factType
		case string(factUniqueStr):
			out |= factUnique
		case string(factPermStr):
			out |= factPerm
		case string(factLangStr):
			out |= factLang
		case string(factMediaTypeStr):
			out |= factMediaType
		case string(factCharsetStr):
			out |= factCharset
		}
		if out != last {
			reply = append(reply, []byte(f)...)
			reply = append(reply, ';')
		}
	}
	return out, reply
}
