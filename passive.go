
package septet

import (
	"net"
	"math/rand"
	"strconv"
	"time"
)

func initPassive(client *Client) error {
	var err error
	var listen net.Listener
	if !conf.PassiveMode {
		panic("passive mode attempted when disabled")
	}

	closePassive(client)

	if conf.PassivePorts == nil {
		listen, err = net.Listen("tcp", ":0")
	} else {
		port := strconv.FormatUint(uint64(conf.PassivePorts[rand.Intn(len(conf.PassivePorts))]), 10)
		listen, err = net.Listen("tcp", ":" + port)
	}
	if err != nil {
		return err
	}
	client.dataListen = listen
	go listenPassive(client)
	return nil
}

func closePassive(client *Client) {
	if client.dataListen != nil {
		client.dataListen.Close()
		client.dataListen = nil
	}

	if client.dataBuffer != nil {
		client.dataBuffer.Close()
		client.dataBuffer = nil
	}
}

func listenPassive(client *Client) {
	for {
		conn, err := client.dataListen.Accept()
		if err != nil {
			// TODO: Handle listener shutdown.
			return
		}
		if client.dataBuffer != nil {
			conn.Close()
		} else {
			client.dataBuffer = conn
		}
	}
}

func waitForPassive(client *Client) {
	start := time.Now().Add(conf.DataTimeout).Unix()
	for start > time.Now().Unix() && client.dataBuffer == nil {
		time.Sleep(time.Millisecond * 50)
	}
	client.data = client.dataBuffer
	client.dataBuffer = nil
}
