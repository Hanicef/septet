
package septet

import (
	"net"
	"strconv"
)

var listenSocket net.Listener
var exitError error

// StartServer starts the listening process and serves all clients connecting
// to the server. The server can only be started once; attepting to start the
// server while it's already running will result in a panic.
func StartServer(config Config) error {
	if listenSocket != nil {
		panic("attepmted to start an already running server")
	}
	err := initConfig(&config)
	if err != nil {
		return err
	}
	conf = config

	listenSocket, err := net.Listen("tcp", ":" + strconv.FormatUint(uint64(config.Port), 10))
	if err != nil {
		return err
	}
	defer StopServer(nil)

	exitError = nil
	for {
		conn, err := listenSocket.Accept()
		if err != nil {
			closeAllClients()
			if exitError != nil {
				return exitError
			} else {
				return err
			}
		}

		client := newClient()
		client.current = &client.root
		client.root.client = client
		client.root.name = "/"
		client.control = conn
		client.allo = -1
		client.dataAddr = conn.RemoteAddr().(*net.TCPAddr)
		client.dataAddr.Port = 21
		client.lang = conf.DefaultLanguage
		go handleClient(client)
	}
}

// StopServer stops the listening process and closes all clients. An error may
// be passed for err, which will be returned from StartServer. This is useful
// if a fatal error occurs outside the package, and required shutting down the
// server with an error.
func StopServer(err error) {
	exitError = err
	if listenSocket != nil {
		listenSocket.Close()
		listenSocket = nil
	}
}
